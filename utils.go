package mimir

import "gitlab.com/gomimir/processor/internal/utils"

type RetryOpts = utils.RetryOpts
type RetryInfo = utils.RetryInfo

var Retry = utils.Retry
