VERSION := $(shell git describe --tags --abbrev=0 | sed -Ee 's/^v|-.*//')
SEMVER_TYPES := major minor patch
BUMP_TARGETS := $(addprefix bump-,$(SEMVER_TYPES))

version:
	@echo Version: $(VERSION)

$(BUMP_TARGETS):
	$(eval bump_type := $(strip $(word 2,$(subst -, ,$@))))
	$(eval f := $(words $(shell a="$(SEMVER_TYPES)"; echo $${a/$(bump_type)*/$(bump_type)} )))
	$(eval newVersion := $(shell echo $(VERSION) | awk -F. -v OFS=. -v f=$(f) '{ $$f++; for (i = $f + 1; i <= 3; i++) $$i = 0 } 1'))
	@echo "Bumping $(VERSION) -> $(newVersion)"
	git tag v$(newVersion)

test:
	go test ./...

vet:
	go vet ./...

pbgen:
	protoc --go_out . --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative protobuf/mimir.proto

.PHONY: test vet version pbgen $(BUMP_TARGETS)