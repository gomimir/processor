// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.19.4
// source: protobuf/mimir.proto

package mimirpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// MimirClient is the client API for Mimir service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type MimirClient interface {
	ServerVersion(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*ServerVersionReply, error)
	UpdateAssets(ctx context.Context, in *UpdateAssetsRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
	UploadSidecars(ctx context.Context, in *UploadSidecarsRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type mimirClient struct {
	cc grpc.ClientConnInterface
}

func NewMimirClient(cc grpc.ClientConnInterface) MimirClient {
	return &mimirClient{cc}
}

func (c *mimirClient) ServerVersion(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*ServerVersionReply, error) {
	out := new(ServerVersionReply)
	err := c.cc.Invoke(ctx, "/mimir.Mimir/ServerVersion", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimirClient) UpdateAssets(ctx context.Context, in *UpdateAssetsRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/mimir.Mimir/UpdateAssets", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimirClient) UploadSidecars(ctx context.Context, in *UploadSidecarsRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/mimir.Mimir/UploadSidecars", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MimirServer is the server API for Mimir service.
// All implementations must embed UnimplementedMimirServer
// for forward compatibility
type MimirServer interface {
	ServerVersion(context.Context, *emptypb.Empty) (*ServerVersionReply, error)
	UpdateAssets(context.Context, *UpdateAssetsRequest) (*emptypb.Empty, error)
	UploadSidecars(context.Context, *UploadSidecarsRequest) (*emptypb.Empty, error)
	mustEmbedUnimplementedMimirServer()
}

// UnimplementedMimirServer must be embedded to have forward compatible implementations.
type UnimplementedMimirServer struct {
}

func (UnimplementedMimirServer) ServerVersion(context.Context, *emptypb.Empty) (*ServerVersionReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ServerVersion not implemented")
}
func (UnimplementedMimirServer) UpdateAssets(context.Context, *UpdateAssetsRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateAssets not implemented")
}
func (UnimplementedMimirServer) UploadSidecars(context.Context, *UploadSidecarsRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UploadSidecars not implemented")
}
func (UnimplementedMimirServer) mustEmbedUnimplementedMimirServer() {}

// UnsafeMimirServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to MimirServer will
// result in compilation errors.
type UnsafeMimirServer interface {
	mustEmbedUnimplementedMimirServer()
}

func RegisterMimirServer(s grpc.ServiceRegistrar, srv MimirServer) {
	s.RegisterService(&Mimir_ServiceDesc, srv)
}

func _Mimir_ServerVersion_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimirServer).ServerVersion(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/mimir.Mimir/ServerVersion",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimirServer).ServerVersion(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _Mimir_UpdateAssets_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateAssetsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimirServer).UpdateAssets(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/mimir.Mimir/UpdateAssets",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimirServer).UpdateAssets(ctx, req.(*UpdateAssetsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Mimir_UploadSidecars_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UploadSidecarsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimirServer).UploadSidecars(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/mimir.Mimir/UploadSidecars",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimirServer).UploadSidecars(ctx, req.(*UploadSidecarsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// Mimir_ServiceDesc is the grpc.ServiceDesc for Mimir service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Mimir_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "mimir.Mimir",
	HandlerType: (*MimirServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ServerVersion",
			Handler:    _Mimir_ServerVersion_Handler,
		},
		{
			MethodName: "UpdateAssets",
			Handler:    _Mimir_UpdateAssets_Handler,
		},
		{
			MethodName: "UploadSidecars",
			Handler:    _Mimir_UploadSidecars_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "protobuf/mimir.proto",
}
