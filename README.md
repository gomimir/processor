# Mimir processor library

This is a shared library used across all official Mimir processors (and even the Mimir server).
It defines common data structures and abstracts queue processing.

You can use it to bootstrap your own Mimir processors so that you don't have to write all of that yourself.

For more information have a look at the [documentation](https://gomimir.gitlab.io/processors/writing-your-own).

## Protobuf generator

https://grpc.io/docs/languages/go/quickstart/

```bash
go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2
```

Update your PATH to contain your Go binaries (if you have not done that already in `.zprofile` or similar)

```bash
export PATH="$PATH:$(go env GOPATH)/bin"
```

```bash
make pbgen
```