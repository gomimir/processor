package mimirpbconv

import (
	"fmt"
	"strings"

	mimir "gitlab.com/gomimir/processor"
	mimirpb "gitlab.com/gomimir/processor/protobuf"
)

func VideoDescFromProtoBuf(desc *mimirpb.VideoDescriptor) (*mimir.VideoDescriptor, error) {
	format, err := VideoFormatFromProtoBuf(desc.Format)
	if err != nil {
		return nil, err
	}

	return &mimir.VideoDescriptor{
		Format:     format,
		Width:      int(desc.Width),
		Height:     int(desc.Height),
		VideoCodec: desc.VideoCodec,
		AudioCodec: desc.AudioCodec,
	}, nil
}

func VideoDescToProtoBuf(desc mimir.VideoDescriptor) (*mimirpb.VideoDescriptor, error) {
	format, err := VideoFormatToProtoBuf(desc.Format)
	if err != nil {
		return nil, err
	}

	return &mimirpb.VideoDescriptor{
		Format:     format,
		Width:      int64(desc.Width),
		Height:     int64(desc.Height),
		VideoCodec: desc.VideoCodec,
		AudioCodec: desc.AudioCodec,
	}, nil
}

func VideoFormatFromProtoBuf(format mimirpb.VideoFormat) (mimir.VideoFileFormat, error) {
	out := mimir.VideoFileFormat(strings.ToLower(format.String()))
	if out.IsValid() {
		return out, nil
	}

	return out, fmt.Errorf("invalid video format: %v", format.String())
}

func VideoFormatToProtoBuf(format mimir.VideoFileFormat) (mimirpb.VideoFormat, error) {
	if value, ok := mimirpb.VideoFormat_value[strings.ToUpper(string(format))]; ok {
		return mimirpb.VideoFormat(value), nil
	}

	return mimirpb.VideoFormat_MP4, fmt.Errorf("cannot convert video format %v to protobuf", format.DisplayName())
}
