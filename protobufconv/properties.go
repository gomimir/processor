package mimirpbconv

import (
	"encoding/json"
	"fmt"
	reflect "reflect"
	"time"

	"gitlab.com/gomimir/processor/internal/properties"
	mimirpb "gitlab.com/gomimir/processor/protobuf"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
)

// PropertiesFromProtoBuf - Loads properties form protobuf structures
func PropertiesFromProtoBuf(from []*mimirpb.PropertyPair) (*properties.Properties, error) {
	props := &properties.Properties{}

	for _, pair := range from {
		switch v := pair.GetValue().(type) {
		case *mimirpb.PropertyPair_TextValue:
			props.Set(pair.Name, v.TextValue)
		case *mimirpb.PropertyPair_IntValue:
			props.Set(pair.Name, v.IntValue)
		case *mimirpb.PropertyPair_UintValue:
			props.Set(pair.Name, v.UintValue)
		case *mimirpb.PropertyPair_DoubleValue:
			props.Set(pair.Name, v.DoubleValue)
		case *mimirpb.PropertyPair_FractionValue:
			props.Set(pair.Name, properties.Fraction{Numerator: v.FractionValue.Numerator, Denominator: v.FractionValue.Denominator})
		case *mimirpb.PropertyPair_GpsValue:
			props.Set(pair.Name, properties.GPS{Latitude: v.GpsValue.Latitude, Longitude: v.GpsValue.Longitude})
		case *mimirpb.PropertyPair_TimeValue:
			props.Set(pair.Name, v.TimeValue.AsTime())
		default:
			return nil, fmt.Errorf("unable to convert value %v into property type (key: %v)", reflect.TypeOf(v), pair.Name)
		}
	}

	return props, nil
}

func PropertiesToProtoBuf(props properties.Properties) ([]*mimirpb.PropertyPair, error) {
	out := make([]*mimirpb.PropertyPair, len(props.Pairs()))

	for i, pair := range props.Pairs() {
		out[i] = &mimirpb.PropertyPair{
			Name: pair.Name,
		}

		switch v := pair.Value.(type) {
		case string:
			out[i].Value = &mimirpb.PropertyPair_TextValue{TextValue: v}

		case int:
			out[i].Value = &mimirpb.PropertyPair_IntValue{IntValue: int64(v)}

		case int8:
			out[i].Value = &mimirpb.PropertyPair_IntValue{IntValue: int64(v)}

		case int16:
			out[i].Value = &mimirpb.PropertyPair_IntValue{IntValue: int64(v)}

		case int32:
			out[i].Value = &mimirpb.PropertyPair_IntValue{IntValue: int64(v)}

		case int64:
			out[i].Value = &mimirpb.PropertyPair_IntValue{IntValue: v}

		case uint:
			out[i].Value = &mimirpb.PropertyPair_UintValue{UintValue: uint64(v)}

		case uint8:
			out[i].Value = &mimirpb.PropertyPair_UintValue{UintValue: uint64(v)}

		case uint16:
			out[i].Value = &mimirpb.PropertyPair_UintValue{UintValue: uint64(v)}

		case uint32:
			out[i].Value = &mimirpb.PropertyPair_UintValue{UintValue: uint64(v)}

		case uint64:
			out[i].Value = &mimirpb.PropertyPair_UintValue{UintValue: v}

		case float32:
			out[i].Value = &mimirpb.PropertyPair_DoubleValue{DoubleValue: float64(v)}

		case float64:
			out[i].Value = &mimirpb.PropertyPair_DoubleValue{DoubleValue: v}

		case json.Number:
			vf, err := v.Float64()
			if err != nil {
				return nil, err
			}
			out[i].Value = &mimirpb.PropertyPair_DoubleValue{DoubleValue: vf}

		case properties.Fraction:
			out[i].Value = &mimirpb.PropertyPair_FractionValue{
				FractionValue: &mimirpb.PropertyPair_Fraction{
					Numerator:   v.Numerator,
					Denominator: v.Denominator,
				},
			}

		case properties.GPS:
			out[i].Value = &mimirpb.PropertyPair_GpsValue{
				GpsValue: &mimirpb.PropertyPair_GPS{
					Latitude:  v.Latitude,
					Longitude: v.Longitude,
				},
			}

		case time.Time:
			out[i].Value = &mimirpb.PropertyPair_TimeValue{
				TimeValue: timestamppb.New(v),
			}

		default:
			return nil, fmt.Errorf("unable to convert value %v into property type (key: %v)", reflect.TypeOf(v), pair.Name)
		}
	}

	return out, nil
}
