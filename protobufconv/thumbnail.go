package mimirpbconv

import (
	"fmt"
	"strings"

	mimir "gitlab.com/gomimir/processor"
	mimirpb "gitlab.com/gomimir/processor/protobuf"
)

func ThumbnailDescFromProtoBuf(desc *mimirpb.ThumbnailDescriptor) (*mimir.ThumbnailDescriptor, error) {
	format, err := ThumbnailFormatFromProtoBuf(desc.Format)
	if err != nil {
		return nil, err
	}

	return &mimir.ThumbnailDescriptor{
		Format: format,
		Width:  int(desc.Width),
		Height: int(desc.Height),
	}, nil
}

func ThumbnailDescToProtoBuf(desc mimir.ThumbnailDescriptor) (*mimirpb.ThumbnailDescriptor, error) {
	format, err := ThumbnailFormatToProtoBuf(desc.Format)
	if err != nil {
		return nil, err
	}

	return &mimirpb.ThumbnailDescriptor{
		Format: format,
		Width:  int64(desc.Width),
		Height: int64(desc.Height),
	}, nil
}

func ThumbnailFormatFromProtoBuf(format mimirpb.ThumbnailFormat) (mimir.ThumbnailFileFormat, error) {
	out := mimir.ThumbnailFileFormat(strings.ToLower(format.String()))
	if out.IsValid() {
		return out, nil
	}

	return out, fmt.Errorf("invalid thumbnail format: %v", format.String())
}

func ThumbnailFormatToProtoBuf(format mimir.ThumbnailFileFormat) (mimirpb.ThumbnailFormat, error) {
	if value, ok := mimirpb.ThumbnailFormat_value[strings.ToUpper(string(format))]; ok {
		return mimirpb.ThumbnailFormat(value), nil
	}

	return mimirpb.ThumbnailFormat_JPEG, fmt.Errorf("cannot convert thumbnail format %v to protobuf", format.DisplayName())
}
