package mimir

// VideoDescriptor - set of properties uniquely describing transcoded video
type VideoDescriptor struct {
	Format VideoFileFormat `json:"format"`

	Width  int `json:"width"`
	Height int `json:"height"`

	VideoCodec string `json:"vcodec"`
	AudioCodec string `json:"acodec"`
}

// Equal - returns true if the descriptor equals to another
func (desc VideoDescriptor) Equal(another VideoDescriptor) bool {
	return desc.Width == another.Width &&
		desc.Height == another.Height &&
		desc.Format == another.Format &&
		desc.VideoCodec == another.VideoCodec &&
		desc.AudioCodec == another.AudioCodec
}

// ScoreDimensionFit - Calculate how close is the thumbnail to given dimensions
// Returns score in interval < -1, 1 >, if one of the requested dimensions is greater than the actual, it is <0
// 1 is considered the best fit
func (desc VideoDescriptor) ScoreDimensionFit(width, height *int) float64 {
	return scoreDimensionsFit(desc.Width, desc.Height, width, height)
}
