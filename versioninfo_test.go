package mimir_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	mimir "gitlab.com/gomimir/processor"
)

func TestVersionInfoFromStr(t *testing.T) {
	testStr := "1.2.3-next-b96e472-20210320"

	verInfo, err := mimir.VersionInfoFromStr(testStr)
	if assert.NoError(t, err) {
		assert.Equal(t, 1, verInfo.Major)
		assert.Equal(t, 2, verInfo.Minor)
		assert.Equal(t, 3, verInfo.Patch)
		assert.Equal(t, testStr, fmt.Sprintf("%v", verInfo))
		assert.True(t, verInfo.IsValid())
	}
}

func TestVersionInfoGTE(t *testing.T) {
	assert.True(t, mustParse(t, "1.0.0").GTE(mustParse(t, "0.9.1")))
	assert.True(t, mustParse(t, "1.0.0").GTE(mustParse(t, "1.0.0")))
}

func mustParse(t *testing.T, str string) mimir.VersionInfo {
	verInfo, err := mimir.VersionInfoFromStr("1.0.0")

	if !assert.NoError(t, err) {
		t.FailNow()
	}

	return verInfo
}
