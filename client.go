package mimir

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"

	mimirpb "gitlab.com/gomimir/processor/protobuf"
)

type Client interface {
	mimirpb.MimirClient
	GetFileReader(ctx context.Context, fileRef FileRef) (io.ReadCloser, error)
	GetFileBytes(ctx context.Context, fileRef FileRef) ([]byte, error)
}

type client struct {
	mimirpb.MimirClient
	httpBaseURL string
	httpClient  *http.Client
}

func NewClient(grpc mimirpb.MimirClient, serverURL string) Client {
	netTransport := &http.Transport{
		Dial: (&net.Dialer{
			Timeout: 5 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 5 * time.Second,
	}

	return &client{
		MimirClient: grpc,
		httpBaseURL: serverURL,
		httpClient: &http.Client{
			// This timeout is for the full request, including reading the body.
			// We do not want to timeout globally in case of huge files.
			// Individual call timeout is handled by the context.
			Timeout:   0,
			Transport: netTransport,
		},
	}
}

// GetFileReader - file reader from remote repository
func (c *client) GetFileReader(ctx context.Context, fileRef FileRef) (io.ReadCloser, error) {
	tokens := strings.Split(fileRef.Filename, "/")
	for i, token := range tokens {
		tokens[i] = url.PathEscape(token)
	}

	url := fmt.Sprintf("%v/file/%v/%v", c.httpBaseURL, url.PathEscape(fileRef.Repository), strings.Join(tokens, "/"))
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	res, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != 200 {
		return nil, fmt.Errorf("server responded with unexpected code %v", res.Status)
	}

	return res.Body, nil
}

// GetFileBytes - retrieves whole file into memory
func (c *client) GetFileBytes(ctx context.Context, fileRef FileRef) ([]byte, error) {
	file, err := c.GetFileReader(ctx, fileRef)
	if err != nil {
		return nil, err
	}

	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}
