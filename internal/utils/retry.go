package utils

import (
	"context"
	"fmt"
	"time"
)

type RetryInfo struct {
	Try          int
	WillTryAgain bool
	NextCooldown time.Duration
}

type RetryOpts struct {
	MaxTries int
	Cooldown []time.Duration
}

func (opts RetryOpts) ApplyDefaults() RetryOpts {
	if len(opts.Cooldown) == 0 {
		opts.Cooldown = []time.Duration{1 * time.Second, 1 * time.Second, 2 * time.Second, 5 * time.Second}
	}

	if opts.MaxTries == 0 {
		opts.MaxTries = 10
	}

	return opts
}

func Retry(ctx context.Context, handler func(context.Context, RetryInfo) error, opts RetryOpts) error {
	var err error
	for i := 1; i <= opts.MaxTries || opts.MaxTries < 0; i++ {
		nextCooldown := 1 * time.Second
		if len(opts.Cooldown) > 0 {
			if i-1 < len(opts.Cooldown) {
				nextCooldown = opts.Cooldown[i-1]
			} else {
				nextCooldown = opts.Cooldown[len(opts.Cooldown)-1]
			}
		}

		err = handler(ctx, RetryInfo{
			Try:          i,
			WillTryAgain: opts.MaxTries < 0 || i < opts.MaxTries,
			NextCooldown: nextCooldown,
		})

		if err == nil {
			return nil
		}

		cooldownT := time.NewTimer(nextCooldown)

		select {
		case <-cooldownT.C:
			continue
		case <-ctx.Done():
			cooldownT.Stop()
			return ctx.Err()
		}
	}

	return fmt.Errorf("gave up after %v tries: %v", opts.MaxTries, err)
}
