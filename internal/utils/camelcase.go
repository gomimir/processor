package utils

import (
	"regexp"
	"strings"
)

var keyRx = regexp.MustCompile(`^[A-Z]+`)

// CamelCase - converts name of the property to camelCase
func CamelCase(name string) string {
	prefix := keyRx.FindString(name)
	if prefix == "" {
		return name
	}

	if len(prefix) == len(name) {
		return strings.ToLower(name)
	}

	if len(prefix) == 1 {
		return strings.ToLower(name[0:1]) + name[1:]
	}

	return strings.ToLower(name[0:len(prefix)-1]) + name[len(prefix)-1:]
}
