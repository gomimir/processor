package properties

import (
	"encoding/json"
	"fmt"
	"reflect"
	"sort"
	"strings"

	"gitlab.com/gomimir/processor/internal/utils"
)

// Properties - map containing generic properties
type Properties struct {
	data []PropertyPair
}

// FromStruct - creates properties from struct
func FromStruct(data interface{}) *Properties {
	props := &Properties{}
	props.MergeStruct(data)
	return props
}

// FromMap - creates properties from value map
func FromMap(data map[string]interface{}) *Properties {
	props := &Properties{}
	props.MergeMap(data)
	return props
}

// FromSerializedPairs - creates properties from serialized list of key/value pairs
func FromSerializedPairs(data []map[string]interface{}) (*Properties, error) {
	props := &Properties{}

	for idx, pairData := range data {
		pair := PropertyPair{}
		err := UnserializePropertyPair(pairData, &pair)
		if err != nil {
			return nil, fmt.Errorf("unable to process property pair at %v: %v", idx, err.Error())
		}

		props.Set(pair.Name, pair.Value)
	}

	return props, nil
}

// FromSerializedMap - creates properties from serialized map of values
func FromSerializedMap(data map[string]interface{}) (*Properties, error) {
	props := &Properties{}

	for k, v := range data {
		if valueMap, ok := v.(map[string]interface{}); ok {
			unserialized, err := UnserializePropertyValue(valueMap)
			if err != nil {
				return nil, fmt.Errorf("invalid property value for key %v: %v", k, err.Error())
			}

			props.Set(k, unserialized)
		} else {
			return nil, fmt.Errorf("invalid property value for key %v: expected object", k)
		}
	}

	return props, nil
}

// MergeStruct - merges struct into properties, overriding any previously set values
func (props *Properties) MergeStruct(data interface{}) {
	if data != nil {
		dataValue := reflect.ValueOf(data)
		if dataValue.Type().Kind() == reflect.Ptr {
			dataValue = dataValue.Elem()
		}

		dataType := dataValue.Type()
		if dataType.Kind() == reflect.Struct {
			for i := 0; i < dataValue.NumField(); i++ {
				fieldType := dataType.Field(i)
				fieldValue := dataValue.Field(i)

				if fieldType.Type.Kind() == reflect.Ptr {
					if fieldValue.IsNil() {
						continue
					} else {
						fieldValue = fieldValue.Elem()
					}
				}

				key := utils.CamelCase(fieldType.Name)
				if mimirTag := fieldType.Tag.Get("mimir"); mimirTag != "" {
					key = mimirTag
				}

				props.Set(key, fieldValue.Interface())
			}
		} else {
			panic(fmt.Errorf("unexpected type %v", dataType))
		}
	}
}

// MergeMap - merges map of values into properties, overriding any previously set values
func (props *Properties) MergeMap(data map[string]interface{}) {
	for k, v := range data {
		props.Set(k, v)
	}
}

// Set - sets key to given value
func (props *Properties) Set(key string, value interface{}) {
	for i, pair := range props.data {
		if pair.Name == key {
			props.data[i].Value = value
			return
		}
	}

	props.data = append(props.data, PropertyPair{
		Name:  key,
		Value: value,
	})
}

// Unset - removes property with given key
func (props *Properties) Unset(key string) {
	for idx, pair := range props.data {
		if pair.Name == key {
			props.data = append(props.data[:idx], props.data[idx+1:]...)
			return
		}
	}
}

// Get - retrieves given key
func (props *Properties) Get(key string) interface{} {
	for _, pair := range props.data {
		if pair.Name == key {
			return pair.Value
		}
	}

	return nil
}

// ToMap - returns properties as plain map
func (props *Properties) ToMap() map[string]interface{} {
	result := make(map[string]interface{})

	for _, pair := range props.data {
		result[pair.Name] = pair.Value
	}

	return result
}

func (props *Properties) Pairs() []PropertyPair {
	return props.data
}

// ToSerializableMap - serializes properties into map of serialized values
// (ie. for storing them in a document)
func (props *Properties) ToSerializableMap() (map[string]interface{}, error) {
	result := make(map[string]interface{})

	for _, pair := range props.data {
		serialized, err := SerializePropertyValue(pair.Value)
		if err != nil {
			return nil, err
		}

		result[pair.Name] = serialized
	}

	return result, nil
}

// ToSerializablePairs - serializes properties into array of property pairs
// (ie. for sending them in GraphQL payload)
func (props Properties) ToSerializablePairs() ([]map[string]interface{}, error) {
	var result []map[string]interface{}

	for _, pair := range props.data {
		serialized, err := SerializePropertyValue(pair.Value)
		if err != nil {
			return nil, err
		}

		serialized["name"] = pair.Name
		result = append(result, serialized)
	}

	return result, nil
}

// MarshalJSON - custom JSON marshaling (using serialized map)
func (props Properties) MarshalJSON() ([]byte, error) {
	serialized, err := props.ToSerializablePairs()
	if err != nil {
		return nil, err
	}

	return json.Marshal(serialized)
}

// UnmarshalJSON - custom JSON unmarshaling (using serialized map)
func (props *Properties) UnmarshalJSON(b []byte) error {
	var data []map[string]interface{}
	err := json.Unmarshal(b, &data)

	if err != nil {
		return err
	}

	unmarshalledProps, err := FromSerializedPairs(data)
	if err != nil {
		return err
	}

	props.data = unmarshalledProps.data
	return nil
}

// Clone - creates new properties map containing all properties from this one
func (props *Properties) Clone() *Properties {
	newData := make([]PropertyPair, len(props.data))
	copy(newData, props.data)

	return &Properties{data: newData}
}

// String - outputs debug dump as a string
func (props *Properties) String() string {
	var lines []string
	for _, pair := range props.data {
		serialized, err := SerializePropertyValue(pair.Value)
		if err == nil && len(serialized) == 1 {
			for serializedK, serializedV := range serialized {
				if f, ok := pair.Value.(Fraction); ok {
					lines = append(lines, fmt.Sprintf("%v(%v): %v/%v", pair.Name, serializedK, f.Numerator, f.Denominator))
				} else {
					marshalled, err := json.Marshal(serializedV)
					if err != nil {
						lines = append(lines, fmt.Sprintf("%v(invalid): %#v", pair.Name, pair.Value))
					} else {
						lines = append(lines, fmt.Sprintf("%v(%v): %v", pair.Name, serializedK, string(marshalled)))
					}
				}
			}
		} else {
			lines = append(lines, fmt.Sprintf("%v(invalid): %#v", pair.Name, pair.Value))
		}
	}

	if len(lines) == 0 {
		return "no properties"
	}

	sort.Strings(lines)

	return strings.Join(lines, "\n")
}

func (props Properties) Equal(another *Properties) bool {
	if len(props.data) != len(another.data) {
		return false
	}

	for _, pair := range props.data {
		v := pair.Value
		v2 := another.Get(pair.Name)

		if reflect.TypeOf(v) == reflect.TypeOf(v2) {
			switch v := v.(type) {
			case Fraction:
				if !v.Equal(v2.(Fraction)) {
					return false
				}
			case GPS:
				if !v.Equal(v2.(GPS)) {
					return false
				}
			default:
				if v != v2 {
					return false
				}
			}
		} else {
			if vF, err := getFloat(v); err == nil {
				if vF2, err := getFloat(v2); err == nil {
					if vF == vF2 {
						continue
					}
				}
			}

			return false
		}
	}

	return true
}

// PropertyPair - pair containing property name an unserialized property values
type PropertyPair struct {
	Name  string
	Value interface{}
}

var floatType = reflect.TypeOf(float64(0))

func getFloat(unk interface{}) (float64, error) {
	v := reflect.ValueOf(unk)
	v = reflect.Indirect(v)
	if !v.Type().ConvertibleTo(floatType) {
		return 0, fmt.Errorf("cannot convert %v to float64", v.Type())
	}
	fv := v.Convert(floatType)
	return fv.Float(), nil
}
