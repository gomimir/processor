package properties

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/mitchellh/mapstructure"
)

// UnserializePropertyPair - unserializes property k/v pair
func UnserializePropertyPair(data map[string]interface{}, pair *PropertyPair) error {
	if name, ok := data["name"]; ok {
		if nameStr, ok := name.(string); ok && name != "" {
			unserialized, err := UnserializePropertyValue(data)
			if err != nil {
				return err
			}

			pair.Name = nameStr
			pair.Value = unserialized

			return nil
		}

		return errors.New("invalid property name")
	}

	return errors.New("missing property name")
}

// UnserializePropertyValue - reads the data payload and tries to unserialize value back to original object
func UnserializePropertyValue(data map[string]interface{}) (interface{}, error) {
	var value interface{}

	for k, v := range data {
		if !strings.HasSuffix(k, "Value") || v == nil {
			continue
		}

		if value != nil {
			return nil, errors.New("unable to parse property payload, multiple values found")
		}

		switch k {
		case "textValue":
			value = v
		case "numericValue":
			value = v
		case "gpsValue":
			var gps GPS
			if err := mapstructure.Decode(v, &gps); err != nil {
				return nil, err
			}

			value = gps
		case "fractionValue":
			var fraction Fraction
			if err := mapstructure.Decode(v, &fraction); err != nil {
				return nil, err
			}

			value = fraction
		case "timeValue":
			if str, ok := v.(string); ok {
				t, err := time.Parse(time.RFC3339, str)
				if err != nil {
					return nil, err
				}

				value = t
			} else {
				return nil, fmt.Errorf("unable to parse time value, expected string, got: %v", reflect.TypeOf(v).Name())
			}

		}
	}

	return value, nil
}

// SerializePropertyValue - serializes value of single property into object usable in GraphQL payload
func SerializePropertyValue(value interface{}) (map[string]interface{}, error) {
	prop := map[string]interface{}{}

	switch v := value.(type) {
	case string:
		prop["textValue"] = v

	case int:
		prop["numericValue"] = float64(v)

	case int8:
		prop["numericValue"] = float64(v)

	case int16:
		prop["numericValue"] = float64(v)

	case int32:
		prop["numericValue"] = float64(v)

	case int64:
		prop["numericValue"] = float64(v)

	case uint:
		prop["numericValue"] = float64(v)

	case uint8:
		prop["numericValue"] = float64(v)

	case uint16:
		prop["numericValue"] = float64(v)

	case uint32:
		prop["numericValue"] = float64(v)

	case uint64:
		prop["numericValue"] = float64(v)

	case float32:
		prop["numericValue"] = float64(v)

	case float64:
		prop["numericValue"] = float64(v)

	case json.Number:
		var err error
		prop["numericValue"], err = v.Float64()
		if err != nil {
			return nil, err
		}

	case Fraction:
		prop["fractionValue"] = map[string]interface{}{
			"numerator":   float64(v.Numerator),
			"denominator": float64(v.Denominator),
		}

	case GPS:
		prop["gpsValue"] = map[string]interface{}{
			"latitude":  v.Latitude,
			"longitude": v.Longitude,
		}

	case time.Time:
		prop["timeValue"] = v.Format(time.RFC3339)

	default:
		return nil, fmt.Errorf("unable to serialize type %v into property payload", reflect.TypeOf(value))
	}

	return prop, nil
}
