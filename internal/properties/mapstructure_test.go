package properties_test

import (
	"testing"

	"github.com/mitchellh/mapstructure"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/processor/internal/properties"
)

func TestMapstructureDecodeHookWithMap(t *testing.T) {
	var decoded struct {
		Properties properties.Properties
	}

	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		DecodeHook: properties.SerializedPropertiesDecodeHook,
		Result:     &decoded,
	})

	if assert.NoError(t, err) {
		err = decoder.Decode(map[string]interface{}{
			"properties": map[string]interface{}{
				"someText": map[string]interface{}{
					"textValue": "Hello world",
				},
			},
		})

		if assert.NoError(t, err) {
			assert.Equal(t, "Hello world", decoded.Properties.Get("someText"))
		}
	}
}

func TestMapstructureDecodeHookWithPairs(t *testing.T) {
	var decoded struct {
		Properties properties.Properties
	}

	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		DecodeHook: properties.SerializedPropertiesDecodeHook,
		Result:     &decoded,
	})

	if assert.NoError(t, err) {
		err = decoder.Decode(map[string]interface{}{
			"properties": []map[string]interface{}{
				{
					"name":      "someText",
					"textValue": "Hello world",
				},
			},
		})

		if assert.NoError(t, err) {
			assert.Equal(t, "Hello world", decoded.Properties.Get("someText"))
		}
	}
}
