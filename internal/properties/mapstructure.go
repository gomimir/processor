package properties

import (
	"fmt"
	"reflect"
)

// SerializedPropertiesDecodeHook - hook for mapstructure for decoding serialized property maps / lists of pairs
func SerializedPropertiesDecodeHook(from reflect.Type, to reflect.Type, data interface{}) (interface{}, error) {
	if to == reflect.TypeOf(Properties{}) {
		if fromMap, ok := data.(map[string]interface{}); ok {
			props, err := FromSerializedMap(fromMap)
			if err != nil {
				return nil, err
			}

			return *props, nil
		} else if fromPairs, ok := data.([]map[string]interface{}); ok {
			props, err := FromSerializedPairs(fromPairs)
			if err != nil {
				return nil, err
			}

			return *props, nil
		} else if list, ok := data.([]interface{}); ok {
			props := Properties{}
			for idx, pairIface := range list {
				if pairData, ok := pairIface.(map[string]interface{}); ok {
					pair := PropertyPair{}
					err := UnserializePropertyPair(pairData, &pair)
					if err != nil {
						return nil, err
					}

					props.Set(pair.Name, pair.Value)
				} else {
					return nil, fmt.Errorf("invalid properties pair at %v", idx)
				}
			}

			return props, nil
		}

		return nil, fmt.Errorf("invalid properties map: %#v", data)
	}

	return data, nil
}
