package properties_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/processor/internal/properties"
)

func TestStringProperty(t *testing.T) {
	original := "hello world"
	unserialized := testPropertySerialization(t, original)

	assert.Equal(t, original, unserialized)
}

func TestNumericProperties(t *testing.T) {
	input := []interface{}{
		int(1), int8(2), int16(3), int32(4), int64(5),
		uint(1), uint8(2), uint16(3), uint32(4), uint64(5),
		float32(1), float64(1),
	}

	for original := range input {
		unserialized := testPropertySerialization(t, original)
		assert.Equal(t, float64(original), unserialized)
	}
}

func TestFractionProperty(t *testing.T) {
	original := properties.Fraction{
		Numerator:   2,
		Denominator: 3,
	}

	unserialized := testPropertySerialization(t, original)
	assert.Equal(t, original, unserialized)
}

func TestGPSProperty(t *testing.T) {
	original := properties.GPS{
		Latitude:  50.093075567483865,
		Longitude: 14.42235936102159,
	}

	unserialized := testPropertySerialization(t, original)
	assert.Equal(t, original, unserialized)
}

func TestTimeProperty(t *testing.T) {
	original := time.Date(2020, 8, 1, 5, 34, 21, 0, time.UTC)

	unserialized := testPropertySerialization(t, original)
	assert.Equal(t, original, unserialized)
}

func testPropertySerialization(t *testing.T, input interface{}) interface{} {
	serialized, err := properties.SerializePropertyValue(input)
	if assert.NoError(t, err) {
		unserialized, err := properties.UnserializePropertyValue(serialized)
		if assert.NoError(t, err) {
			return unserialized
		}
	}

	// Never gonna happen
	t.FailNow()
	return nil
}

