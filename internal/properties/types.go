package properties

// Fraction - custom property type for holding fractions
type Fraction struct {
	Numerator   int64 `json:"numerator"`
	Denominator int64 `json:"denominator"`
}

func (f Fraction) Equal(another Fraction) bool {
	return f.Denominator == another.Denominator && f.Numerator == another.Numerator
}

// GPS - custom property type for holding GPS coordinates
type GPS struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

func (f GPS) Equal(another GPS) bool {
	return f.Latitude == another.Latitude && f.Longitude == another.Longitude
}
