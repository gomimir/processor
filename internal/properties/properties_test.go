package properties_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gomimir/processor/internal/properties"
)

func TestPropertiesFromStruct(t *testing.T) {
	props := buildTestProperties()

	assert.Equal(t, "Hello world", props.Get("string"))
	assert.Equal(t, float64(25), props.Get("optionalNumber"))
}

func TestPropertiesFromAliasedStruct(t *testing.T) {
	props := properties.FromStruct(struct {
		FooBar string `mimir:"foo"`
	}{
		FooBar: "Hello world",
	})

	assert.Equal(t, "Hello world", props.Get("foo"))
}

func TestPropertiesFromMap(t *testing.T) {
	props := properties.FromMap(map[string]interface{}{
		"someText": "Hello world",
	})

	assert.Equal(t, "Hello world", props.Get("someText"))
}

func TestPropertiesFromSerializedPairs(t *testing.T) {
	props, err := properties.FromSerializedPairs([]map[string]interface{}{
		{
			"name":      "greetings",
			"textValue": "Hello world",
		},
	})

	if assert.NoError(t, err) {
		assert.Equal(t, "Hello world", props.Get("greetings"))
	}
}

func TestPropertiesFromSerializedMap(t *testing.T) {
	props, err := properties.FromSerializedMap(map[string]interface{}{
		"greetings": map[string]interface{}{
			"textValue": "Hello world",
		},
	})

	if assert.NoError(t, err) {
		assert.Equal(t, "Hello world", props.Get("greetings"))
	}
}

func TestPropertiesToSerializablePairs(t *testing.T) {
	pairs, err := buildTestProperties().ToSerializablePairs()

	if assert.NoError(t, err) {
		assert.ElementsMatch(t, []map[string]interface{}{
			{
				"name":      "string",
				"textValue": "Hello world",
			},
			{
				"name":         "optionalNumber",
				"numericValue": float64(25),
			},
		}, pairs)
	}
}

func TestPropertiesToSerializableMap(t *testing.T) {
	pairs, err := buildTestProperties().ToSerializableMap()

	if assert.NoError(t, err) {
		assert.Equal(t, map[string]interface{}{
			"string": map[string]interface{}{
				"textValue": "Hello world",
			},
			"optionalNumber": map[string]interface{}{
				"numericValue": float64(25),
			},
		}, pairs)
	}
}

func TestPropertiesJSONMarshalling(t *testing.T) {
	props := buildTestProperties()
	bytes, err := json.MarshalIndent(props, "", "  ")
	if assert.NoError(t, err) {
		var out properties.Properties
		err = json.Unmarshal(bytes, &out)
		if assert.NoError(t, err) {
			assert.Equal(t, *props, out)
		}
	}
}

func TestPropertiesEqual(t *testing.T) {
	a1 := properties.FromMap(map[string]interface{}{
		"foo": "Hello world",
	})

	b1 := properties.FromMap(map[string]interface{}{
		"foo": "Hello world",
	})

	assert.True(t, a1.Equal(b1))

	a2 := properties.FromMap(map[string]interface{}{
		"foo": "Hello world",
	})

	b2 := properties.FromMap(map[string]interface{}{
		"foo": 55,
	})

	assert.False(t, a2.Equal(b2))

	a3 := properties.FromMap(map[string]interface{}{
		"foo": properties.Fraction{Numerator: 1, Denominator: 3},
	})

	b3 := properties.FromMap(map[string]interface{}{
		"foo": properties.Fraction{Numerator: 1, Denominator: 3},
	})

	assert.True(t, a3.Equal(b3))

	a4 := properties.FromMap(map[string]interface{}{
		"foo": 6,
	})

	b4 := properties.FromMap(map[string]interface{}{
		"foo": 6.0,
	})

	assert.True(t, a4.Equal(b4))
}

func buildTestProperties() *properties.Properties {
	var n float64 = 25

	props := properties.FromStruct(struct {
		String         string
		OptionalNumber *float64
	}{
		String:         "Hello world",
		OptionalNumber: &n,
	})

	return props
}
