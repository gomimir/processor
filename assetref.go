package mimir

import (
	"bytes"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
)

var rawURLEncoding = base64.URLEncoding.WithPadding(base64.NoPadding)

type assetRefPair struct {
	IndexName string `json:"index"`
	AssetID   string `json:"assetId"`
}

type AssetRef struct {
	str     string
	isValid bool

	refPair assetRefPair
}

func AssetRefFromString(serialized string) AssetRef {
	decodedBytes, err := rawURLEncoding.DecodeString(serialized)
	if err == nil {
		idx := bytes.Index(decodedBytes, []byte{':'})

		if idx >= 0 {
			return AssetRef{
				str: serialized,
				refPair: assetRefPair{
					IndexName: string(decodedBytes[0:idx]),
					AssetID:   rawURLEncoding.EncodeToString(decodedBytes[idx+1:]),
				},
				isValid: true,
			}
		}
	}

	return AssetRef{
		str: serialized,
	}
}

func AssetRefForID(indexName string, assetID string) AssetRef {
	assetIDBytes, err := rawURLEncoding.DecodeString(assetID)

	if err != nil {
		return AssetRef{
			refPair: assetRefPair{
				IndexName: indexName,
				AssetID:   assetID,
			},
		}
	}

	idBytes := append([]byte(indexName+":"), assetIDBytes...)
	str := rawURLEncoding.EncodeToString(idBytes)

	return AssetRef{
		str: str,
		refPair: assetRefPair{
			IndexName: indexName,
			AssetID:   assetID,
		},
		isValid: true,
	}
}

func AssetRefForKey(indexName string, assetKey string) AssetRef {
	hash := sha256.Sum224([]byte(assetKey))
	assetID := rawURLEncoding.EncodeToString(hash[:])

	return AssetRefForID(indexName, assetID)
}

func (ref AssetRef) IndexName() string {
	return ref.refPair.IndexName
}

func (ref AssetRef) AssetID() string {
	return ref.refPair.AssetID
}

func (ref AssetRef) IsValid() bool {
	return ref.isValid
}

func (ref AssetRef) String() string {
	return ref.str
}

// MarshalJSON
func (ref AssetRef) MarshalJSON() ([]byte, error) {
	return json.Marshal(ref.refPair)
}

// UnmarshalJSON
func (ref *AssetRef) UnmarshalJSON(b []byte) error {
	err := json.Unmarshal(b, &ref.refPair)
	if err != nil {
		ref.isValid = true
	}

	return err
}
