package mimir

import (
	"gitlab.com/gomimir/processor/internal/properties"
)

// Properties - property map wrapper enhanced with additional methods for working with generic value payloads
type Properties = properties.Properties

// PropertiesFromStruct - constructs properties map from struct
var PropertiesFromStruct = properties.FromStruct

// PropertiesFromMap - constructs properties map from map of values
var PropertiesFromMap = properties.FromMap

// PropertiesFromSerializedPairs - constructs properties map by unserializing serialized key/value pairs
var PropertiesFromSerializedPairs = properties.FromSerializedPairs

// PropertiesFromSerializedMap - constructs properties map by unserializing map of serialized values
var PropertiesFromSerializedMap = properties.FromSerializedMap

// UnserializePropertyPair - unserializes single serialized property pair
var UnserializePropertyPair = properties.UnserializePropertyPair

// PropertyPair - pair containing property name an unserialized property values
type PropertyPair = properties.PropertyPair

// Fraction - property value container for expressing fractions
type Fraction = properties.Fraction

// GPS - property value container for GPS coordinates
type GPS = properties.GPS

// SerializedPropertiesDecodeHook - hook for mapstructure for decoding serialized property maps / lists of pairs
var SerializedPropertiesDecodeHook = properties.SerializedPropertiesDecodeHook
