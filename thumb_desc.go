package mimir

import "math"

// ThumbnailDescriptor - set of properties uniquely describing thumbnail
type ThumbnailDescriptor struct {
	Format ThumbnailFileFormat `json:"format"`
	Width  int                 `json:"width"`
	Height int                 `json:"height"`
}

// Equal - returns true if the descriptor equals to another
func (thumbDesc ThumbnailDescriptor) Equal(another ThumbnailDescriptor) bool {
	return thumbDesc.Width == another.Width && thumbDesc.Height == another.Height && thumbDesc.Format == another.Format
}

// ScoreDimensionFit - Calculate how close is the thumbnail to given dimensions
// Returns score in interval < -1, 1 >, if one of the requested dimensions is greater than the actual, it is <0
// 1 is considered the best fit
func (thumbDesc ThumbnailDescriptor) ScoreDimensionFit(width, height *int) float64 {
	return scoreDimensionsFit(thumbDesc.Width, thumbDesc.Height, width, height)
}

func scoreDimensionsFit(actualWidth, actualHeight int, desiredWidth, desiredHeight *int) float64 {
	if desiredWidth != nil && desiredHeight != nil {
		return math.Min(
			scoreDimensionFit(actualWidth, *desiredWidth),
			scoreDimensionFit(actualHeight, *desiredHeight),
		)
	} else if desiredWidth != nil {
		return scoreDimensionFit(actualWidth, *desiredWidth)
	} else if desiredHeight != nil {
		return scoreDimensionFit(actualHeight, *desiredHeight)
	}

	return 1
}

func scoreDimensionFit(actual, requested int) float64 {
	if actual == requested {
		return 1
	} else if actual < requested {
		return -1 + float64(actual)/float64(requested)
	} else {
		return float64(requested) / float64(actual)
	}
}
