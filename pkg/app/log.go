package app

import (
	"fmt"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type LogFormat string

const (
	LogFormatJSON   LogFormat = "json"
	LogFormatPlain  LogFormat = "plain"
	LogFormatPretty LogFormat = "pretty"

	LogFormatInvalid LogFormat = "invalid"
)

func (lf LogFormat) IsValid() bool {
	switch lf {
	case LogFormatJSON, LogFormatPlain, LogFormatPretty:
		return true
	}

	return false
}

func ParseLogFormat(str string) (LogFormat, error) {
	lf := LogFormat(str)
	if lf.IsValid() {
		return lf, nil
	} else {
		return LogFormatInvalid, fmt.Errorf("invalid log format: %v", str)
	}
}

func ParseLogLevel(str string) (zerolog.Level, error) {
	if str == "" {
		return zerolog.InfoLevel, nil
	} else {
		logLevel, _ := zerolog.ParseLevel(str)
		if logLevel == zerolog.NoLevel {
			return zerolog.NoLevel, fmt.Errorf("unrecognized log level: %v", str)
		}

		return logLevel, nil
	}
}

func InitLoggerRaw(levelStr, formatStr string) error {
	level, err := ParseLogLevel(levelStr)
	if err != nil {
		return err
	}

	format, err := ParseLogFormat(formatStr)
	if err != nil {
		return err
	}

	InitLogger(level, format)
	return nil
}

func InitLogger(level zerolog.Level, format LogFormat) {
	zerolog.SetGlobalLevel(level)

	if format != LogFormatJSON {
		consoleWriter := zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.Stamp}

		if format == LogFormatPlain {
			consoleWriter.NoColor = true
		}

		log.Logger = log.Output(consoleWriter)
	}
}
