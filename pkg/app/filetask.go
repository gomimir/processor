package app

import (
	"errors"
	"time"

	mimir "gitlab.com/gomimir/processor"
)

type FileTask interface {
	mimir.Task
	File() AssetFileRequest
}

type fileTask struct {
	task    mimir.Task
	fileReq AssetFileRequest
	// params  map[string]interface{}
}

func (t fileTask) ID() string                   { return t.task.ID() }
func (t fileTask) Name() string                 { return t.task.Name() }
func (t fileTask) QueuedAt() time.Time          { return t.task.QueuedAt() }
func (t fileTask) Parameters() mimir.Parameters { return t.task.Parameters() }
func (t fileTask) File() AssetFileRequest       { return t.fileReq }

type testFileTask struct {
	fileReq  AssetFileRequest
	taskName string
	queuedAt time.Time
	params   mimir.Parameters
}

func (t testFileTask) ID() string                   { return "00000000-0000-0000-0000-000000000001" }
func (t testFileTask) Name() string                 { return t.taskName }
func (t testFileTask) QueuedAt() time.Time          { return t.queuedAt }
func (t testFileTask) Parameters() mimir.Parameters { return t.params }
func (t testFileTask) File() AssetFileRequest       { return t.fileReq }

type testFileTaskParams struct{}

func (p testFileTaskParams) LoadTo(dst interface{}) error           { return nil }
func (p testFileTaskParams) AsMap() (map[string]interface{}, error) { return nil, nil }

type testJob struct{}

func (p testJob) ID() string   { return "00000000-0000-0000-0000-000000000001" }
func (p testJob) Name() string { return "test" }
func (p testJob) EnqueueTask(opts mimir.EnqueueTaskOpts) (mimir.Task, error) {
	return nil, errors.New("not implemented")
}
