package app

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	mimir "gitlab.com/gomimir/processor"
)

type App struct {
	appInfo mimir.AppInfo
	rootCmd *cobra.Command
}

func NewApp(name string, versionStr string) *App {
	version, _ := mimir.VersionInfoFromStr(versionStr)
	appInfo := mimir.AppInfo{
		Name:    name,
		Version: version,
	}

	rootCmd := &cobra.Command{
		Use: name,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			return InitLoggerRaw(
				viper.GetString("log-level"),
				viper.GetString("log-format"),
			)
		},
	}

	if appInfo.Version.String() != "" {
		rootCmd.Version = appInfo.Version.String()
	} else {
		rootCmd.Version = "0.0.0"
	}

	rootCmd.PersistentFlags().String("log-level", "", "log level (trace, debug, info, warn, error, fatal, panic)")
	rootCmd.PersistentFlags().String("log-format", "pretty", "log format (json, plain, pretty)")

	return &App{
		appInfo: appInfo,
		rootCmd: rootCmd,
	}
}

func (app *App) AppInfo() mimir.AppInfo {
	return app.appInfo
}

func (app *App) RootCommand() *cobra.Command {
	return app.rootCmd
}

func (app *App) Execute() {
	bindFlags(app.rootCmd)
	app.rootCmd.Execute()
}

func bindFlags(cmd *cobra.Command) {
	viper.BindPFlags(cmd.Flags())
	viper.BindPFlags(cmd.PersistentFlags())

	for _, cmd := range cmd.Commands() {
		bindFlags(cmd)
	}
}
