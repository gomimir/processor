package app

import (
	"context"
	"fmt"
	"io"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/processor/internal/utils"
	mimirprocessor "gitlab.com/gomimir/processor/pkg/processor"
	mimirpb "gitlab.com/gomimir/processor/protobuf"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

type AssetFileRequest interface {
	AssetRef() mimir.AssetRef
	FileRef() mimir.FileRef
	Reader() (io.ReadCloser, error)
}
type remoteFileRequest struct {
	Asset  mimir.AssetRef
	File   mimir.FileRef
	Client mimir.Client
	reader io.ReadCloser
	err    error
}

func (req remoteFileRequest) AssetRef() mimir.AssetRef {
	return req.Asset
}

func (req remoteFileRequest) FileRef() mimir.FileRef {
	return req.File
}

func (req remoteFileRequest) Reader() (io.ReadCloser, error) {
	if req.reader == nil && req.err == nil {
		req.reader, req.err = req.Client.GetFileReader(context.Background(), req.File)
	}

	return req.reader, req.err
}

type localFileRequest struct {
	Filename string
	reader   io.ReadCloser
	err      error
}

func (req localFileRequest) AssetRef() mimir.AssetRef {
	return mimir.AssetRefForKey(
		"test",
		req.Filename,
	)
}

func (req localFileRequest) FileRef() mimir.FileRef {
	return mimir.FileRef{
		Repository: "[local]",
		Filename:   req.Filename,
	}
}

func (req localFileRequest) Reader() (io.ReadCloser, error) {
	if req.reader == nil && req.err == nil {
		req.reader, req.err = os.Open(req.Filename)
	}

	return req.reader, req.err
}

type FileProcessorRequest struct {
	Task FileTask
	Job  mimir.ActiveJob

	Client  mimir.Client
	Log     zerolog.Logger
	IsTest  bool
	Command *cobra.Command
}

type RegisteredFileProcessor struct {
	handler   ProcessorHandler
	testCmd   *cobra.Command
	cmdPreRun []func(*cobra.Command)
}

func (rfp *RegisteredFileProcessor) OnCommandPreRun(callback func(*cobra.Command)) {
	rfp.cmdPreRun = append(rfp.cmdPreRun, callback)
}

func (rfp *RegisteredFileProcessor) TestCommand() *cobra.Command {
	return rfp.testCmd
}

func (rfp *RegisteredFileProcessor) commandPreRun(cmd *cobra.Command) {
	for _, callback := range rfp.cmdPreRun {
		callback(cmd)
	}
}

func (rfp *RegisteredFileProcessor) process(req FileProcessorRequest) error {
	req.Log.Info().Msg("Processing file")
	return rfp.handler(req)
}

type ProcessorHandler = func(req FileProcessorRequest) error

type ProcessorApp struct {
	*App
	OnProcessorStart           func()
	processCmd                 *cobra.Command
	testCmd                    *cobra.Command
	handlers                   map[string]*RegisteredFileProcessor
	defaultProcessingQueueName string
}

func NewProcessorApp(name, versionStr string) *ProcessorApp {
	return &ProcessorApp{
		App:      NewApp(name, versionStr),
		handlers: make(map[string]*RegisteredFileProcessor),
	}
}

func (app *ProcessorApp) ProcessCommand() *cobra.Command {
	return app.processCmd
}

func (app *ProcessorApp) SetDefaultProcessingQueue(queueName string) {
	app.defaultProcessingQueueName = queueName
	viper.SetDefault("queue", queueName)

	if app.processCmd != nil {
		app.processCmd.Flag("queue").DefValue = queueName
	}
}

func (app *ProcessorApp) RegisterFileProcessor(taskName string, handler ProcessorHandler) *RegisteredFileProcessor {
	if app.processCmd == nil {
		processCmd := &cobra.Command{
			Use:   "process",
			Short: "Connect to server and start processing queued tasks",
			Run: func(cmd *cobra.Command, args []string) {
				msg := log.Info()
				if app.appInfo.Version.String() != "" {
					msg.Str("version", app.appInfo.Version.String())
				}

				msg.Msg("Starting processor")

				if app.OnProcessorStart != nil {
					app.OnProcessorStart()
				}

				client := mimir.NewClient(
					connect(cmd.Context(), viper.GetString("server-host")),
					fmt.Sprintf("http://%v", viper.GetString("server-host")),
				)

				taskHandlers := make(map[string]mimir.TaskHandler)
				for k, v := range app.handlers {
					handler := v
					taskHandlers[k] = func(req mimir.HandleRequest) error {
						if req.Error() != nil {
							log.Error().Err(req.Error()).Msg("Error reading queued task")
							return req.Error()
						}

						var data struct {
							Asset      mimir.AssetRef
							File       mimir.FileRef
							Parameters map[string]interface{} `mapstructure:",remain"`
						}

						err := req.Task().Parameters().LoadTo(&data)
						if err != nil {
							return err
						}

						fileTask := fileTask{
							task:    req.Task(),
							fileReq: remoteFileRequest{Asset: data.Asset, File: data.File, Client: client},
							// params:  data.Parameters,
						}

						logger := log.With().Str("repository", data.File.Repository).Str("filename", data.File.Filename).Str("task", req.Task().Name()).Str("task_id", req.Task().ID()).Logger()
						processorReq := FileProcessorRequest{
							Task: fileTask,
							Job:  req.Job(),

							Client:  client,
							Log:     logger,
							Command: cmd,
						}

						if err := handler.process(processorReq); err != nil {
							return err
						}

						return nil
					}
				}

				mimir.Retry(cmd.Context(), func(ctx context.Context, info mimir.RetryInfo) error {
					timeoutCtx, cancel := context.WithTimeout(ctx, 2*time.Second)
					defer cancel()

					serverVersionReply, err := client.ServerVersion(timeoutCtx, &emptypb.Empty{})

					if err == nil {
						log.Info().Str("version", serverVersionReply.VersionString).Msg("Server contacted and ready")
						return nil
					} else {
						if info.WillTryAgain {
							log.Warn().Int("try", info.Try).Err(err).Msgf("Unable to contact Mimir server. Will try again in %v.", info.NextCooldown)
						}
					}

					return err
				}, mimir.RetryOpts{
					MaxTries: -1,
				}.ApplyDefaults())

				for _, handler := range app.handlers {
					handler.commandPreRun(cmd)
				}

				mimirprocessor.Run(mimirprocessor.RunOpts{
					Context: cmd.Context(),

					RedisAddr:     viper.GetString("redis-host"),
					RedisPassword: viper.GetString("redis-password"),
					RedisDatabase: viper.GetInt("redis-database"),

					QueueName:    viper.GetString("queue"),
					Processor:    &app.appInfo,
					TaskHandlers: taskHandlers,
				})
			},
		}

		processCmd.Flags().String("server-host", "127.0.0.1:3001", "Mimir server address and port")
		processCmd.Flags().String("redis-host", "localhost:6379", "Redis server address and port")
		processCmd.Flags().Int("redis-database", 0, "Redis database")
		processCmd.Flags().String("redis-password", "", "Redis database password")
		processCmd.Flags().String("queue", app.defaultProcessingQueueName, "Name of the task queue")

		app.rootCmd.AddCommand(processCmd)
		app.processCmd = processCmd
	}

	if app.testCmd == nil {
		app.testCmd = &cobra.Command{
			Use:   "test",
			Short: "Make a test run on a local file",
		}

		app.rootCmd.AddCommand(app.testCmd)
	}

	rfp := &RegisteredFileProcessor{
		handler: handler,
	}

	rfp.testCmd = &cobra.Command{
		Use:  fmt.Sprintf("%v filename", taskName),
		Args: cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			// Override default log level
			if !viper.IsSet("log-level") {
				zerolog.SetGlobalLevel(zerolog.DebugLevel)
			}

			fileTask := testFileTask{
				fileReq:  localFileRequest{Filename: args[0]},
				taskName: taskName,
				queuedAt: time.Now(),
				params:   testFileTaskParams{},
			}

			job := testJob{}

			logger := log.With().Str("filename", args[0]).Logger()

			req := FileProcessorRequest{
				Task:    fileTask,
				Job:     job,
				IsTest:  true,
				Log:     logger,
				Command: cmd,
			}

			rfp.commandPreRun(cmd)

			if err := rfp.process(req); err != nil {
				log.Fatal().Msg("Test run failed")
				os.Exit(1)
			}
		},
	}

	app.testCmd.AddCommand(rfp.testCmd)

	app.handlers[taskName] = rfp

	return rfp
}

func connect(ctx context.Context, addr string) mimirpb.MimirClient {
	log.Info().Str("host", addr).Msg("Contacting Mimir server")

	var grpcConn *grpc.ClientConn
	retryErr := utils.Retry(ctx, func(tryCtx context.Context, ri utils.RetryInfo) error {
		timeoutCtx, cancel := context.WithTimeout(tryCtx, 2*time.Second)
		defer cancel()

		var err error
		grpcConn, err = grpc.DialContext(
			timeoutCtx, addr, grpc.WithInsecure(), grpc.WithBlock(),
		)

		// https://stackoverflow.com/questions/39825671/grpc-go-how-to-know-in-server-side-when-client-closes-the-connection
		// grpcConn.WaitForStateChange()

		if err != nil {
			if ri.WillTryAgain {
				log.Warn().Str("host", addr).Int("try", ri.Try).Err(err).Msgf("Unable to contact Mimir server. Will try again in %v.", ri.NextCooldown)
			}

			return err
		}

		return nil
	}, utils.RetryOpts{}.ApplyDefaults())

	if retryErr != nil {
		log.Fatal().Err(retryErr).Msg("Unable to establish connection to Mimir server")
		os.Exit(1)
	}

	return mimirpb.NewMimirClient(grpcConn)
}
