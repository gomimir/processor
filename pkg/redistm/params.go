package redistm

import "encoding/json"

type Parameters struct {
	bytes []byte
}

func (p *Parameters) LoadTo(dst interface{}) error {
	if len(p.bytes) > 0 {
		return json.Unmarshal(p.bytes, dst)
	}

	return nil
}

func (p *Parameters) AsMap() (map[string]interface{}, error) {
	var m map[string]interface{}
	err := p.LoadTo(&m)
	if err != nil {
		return nil, err
	}

	return m, nil
}

func (p Parameters) MarshalJSON() ([]byte, error) {
	if len(p.bytes) > 0 {
		return p.bytes, nil
	} else {
		return json.Marshal(struct{}{})
	}
}

func (p *Parameters) UnmarshalJSON(src []byte) error {
	p.bytes = make([]byte, len(src))
	copy(p.bytes, src)
	return nil
}
