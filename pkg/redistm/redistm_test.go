package redistm_test

import (
	"context"
	"testing"
	"time"

	"github.com/alicebob/miniredis/v2"
	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/assert"
	mimir "gitlab.com/gomimir/processor"
	redistm "gitlab.com/gomimir/processor/pkg/redistm"
)

func TestRedisTaskManager(t *testing.T) {
	mr, err := miniredis.Run()
	if err != nil {
		panic(err)
	}

	client := redis.NewClient(&redis.Options{
		Addr: mr.Addr(),
	})

	tm := redistm.NewRedisTaskManager(client)

	testTaskManager(t, tm)
}

func testTaskManager(t *testing.T, tm mimir.TaskManager) {
	// Create base test context for everything with 5 second timeout as a safe measure
	testContext, cancelEverything := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelEverything()

	// --------------------------------------------------------------------------
	// 1. Start monitoring for new jobs / state changes in the background process
	//    Log every event we receive into array to check later
	go func() {
		jm, err := tm.MonitorJobs(testContext)
		if !assert.NoError(t, err) {
			return
		}

		for evt := range jm.Subscribe(testContext) {
			if err := evt.Error(); err != nil {
				assert.Equal(t, context.Canceled, err)
			} else {
				if evt.State().Finished() {
					// Lets end the test (and all it's background goroutines)
					// once we successfully receive confirmation about job being finished
					cancelEverything()
				}
			}
		}
	}()

	// 2. Start processing reports about finished tasks
	go tm.ProcessTaskReports(testContext, mimir.ProcessTaskReportsOpts{
		Handler: func(report mimir.TaskReport) error {
			return nil
		},
	})

	// --------------------------------------------------------------------------
	// 3. Start a new job + plan 1 additional task for it during its execution

	queueOpts := mimir.QueueJobOpts{
		Name: "crawl",
		Enqueue: []mimir.EnqueueTaskOpts{
			{
				Queue: "main",
				Task:  "sayHello",
				Parameters: map[string]interface{}{
					"name": "world",
				},
			},
		},
	}

	queueRes, err := tm.QueueJob(context.Background(), queueOpts)

	if !assert.NoError(t, err) {
		return
	}

	job := queueRes.Job
	taskId := queueRes.Tasks[0].ID()

	// --------------------------------------------------------------------------
	// 4. Retrieve list of jobs, there should be an unfinished one

	jobs, err := tm.GetJobs(context.TODO(), mimir.GetJobsOpts{})
	if !assert.NoError(t, err) || !assert.Equal(t, 1, len(jobs)) {
		return
	}

	assert.Equal(t, job.ID(), jobs[0].ID())
	assert.False(t, jobs[0].Finished())

	// --------------------------------------------------------------------------
	// 5. Start processing the task queue
	err = tm.ProcessQueue(testContext, mimir.ProcessQueueOpts{
		QueueName: "main",
		Handler: func(req mimir.HandleRequest) error {
			if req.Error() != nil {
				return req.Error()
			}

			assert.Equal(t, job.ID(), req.Job().ID())
			assert.Equal(t, taskId, req.Task().ID())
			assert.Equal(t, "sayHello", req.Task().Name())

			params, err := req.Task().Parameters().AsMap()
			if assert.NoError(t, err) {
				assert.Equal(t, queueOpts.Enqueue[0].Parameters, params)
			}

			return nil
		},
		OnResolved: func(report mimir.TaskReport) {
			assert.Equal(t, job.ID(), report.Job().ID())
			assert.Equal(t, taskId, report.Task().ID())
			assert.Nil(t, report.Error())
		},
	})

	if !assert.Equal(t, err, context.Canceled) {
		return
	}

	// --------------------------------------------------------------------------
	// 6. Wait for the task is reported as finished (or test times out)

	<-testContext.Done()

	// --------------------------------------------------------------------------
	// 7. Retrieve list of jobs again, it should contain finished job now

	jobs, err = tm.GetJobs(context.TODO(), mimir.GetJobsOpts{})
	if !assert.NoError(t, err) || !assert.Equal(t, 1, len(jobs)) {
		return
	}

	assert.Equal(t, job.ID(), jobs[0].ID())
	assert.True(t, jobs[0].Finished())
}
