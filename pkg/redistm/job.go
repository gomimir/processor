package redistm

import (
	"encoding/json"
	"time"

	mimir "gitlab.com/gomimir/processor"
)

type jobPayload struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Internal bool   `json:"internal,omitempty"`
}

type Job struct {
	payload jobPayload
}

func (j Job) ID() string   { return j.payload.ID }
func (j Job) Name() string { return j.payload.Name }

func (j Job) MarshalJSON() ([]byte, error)  { return json.Marshal(j.payload) }
func (j *Job) UnmarshalJSON(b []byte) error { return json.Unmarshal(b, &j.payload) }

type taskSummaryPayload struct {
	Name   string `json:"taskName"`
	Failed int    `json:"failed"`
	Total  int    `json:"total"`
	Took   int    `json:"took"`
}

type FinishedJobSummary struct {
	payload taskSummaryPayload
}

func (ts FinishedJobSummary) Name() string   { return ts.payload.Name }
func (ts FinishedJobSummary) Finished() bool { return true }
func (ts FinishedJobSummary) Total() int     { return ts.payload.Total }
func (ts FinishedJobSummary) Pending() int   { return 0 }
func (ts FinishedJobSummary) Failed() int    { return ts.payload.Failed }
func (ts FinishedJobSummary) Took() int      { return ts.payload.Took }

func (ts FinishedJobSummary) MarshalJSON() ([]byte, error)  { return json.Marshal(ts.payload) }
func (ts *FinishedJobSummary) UnmarshalJSON(b []byte) error { return json.Unmarshal(b, &ts.payload) }

type jobInfoPayload struct {
	jobPayload
	QueuedAt    time.Time            `json:"queuedAt"`
	FinishedAt  *time.Time           `json:"finishedAt,omitempty"`
	TaskSummary []taskSummaryPayload `json:"tasks,omitempty"`
}

type JobInfo struct {
	payload  jobInfoPayload
	finished bool
}

func (j JobInfo) ID() string             { return j.payload.ID }
func (j JobInfo) Name() string           { return j.payload.Name }
func (j JobInfo) QueuedAt() time.Time    { return j.payload.QueuedAt }
func (j JobInfo) Finished() bool         { return j.finished }
func (j JobInfo) FinishedAt() *time.Time { return j.payload.FinishedAt }
func (j JobInfo) TaskSummary() []mimir.TaskProgress {
	count := len(j.payload.TaskSummary)
	if count > 0 {
		result := make([]mimir.TaskProgress, count)
		for i, payload := range j.payload.TaskSummary {
			result[i] = FinishedJobSummary{payload}
		}

		return result
	}

	return nil
}

func (j JobInfo) MarshalJSON() ([]byte, error)  { return json.Marshal(j.payload) }
func (j *JobInfo) UnmarshalJSON(b []byte) error { return json.Unmarshal(b, &j.payload) }
