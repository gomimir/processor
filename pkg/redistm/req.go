package redistm

import (
	mimir "gitlab.com/gomimir/processor"
)

type QueuedItem struct {
	UnfinishedQueue string
	Payload         string
}

type HandleRequest struct {
	task       Task
	job        mimir.ActiveJob
	error      error
	queuedItem *QueuedItem
	processor  *mimir.AppInfo
}

func (req *HandleRequest) Error() error         { return req.error }
func (req *HandleRequest) Task() mimir.Task     { return req.task }
func (req *HandleRequest) Job() mimir.ActiveJob { return req.job }
