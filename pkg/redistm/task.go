package redistm

import (
	"encoding/json"
	"time"

	"github.com/rs/xid"
	mimir "gitlab.com/gomimir/processor"
)

type taskPayload struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Job  Job    `json:"job"`

	QueuedAt time.Time `json:"queuedAt"`

	Parameters Parameters `json:"params,omitempty"`
}

type Task struct {
	payload taskPayload
}

func (t Task) ID() string                   { return t.payload.ID }
func (t Task) Name() string                 { return t.payload.Name }
func (t Task) QueuedAt() time.Time          { return t.payload.QueuedAt }
func (t Task) Parameters() mimir.Parameters { return &t.payload.Parameters }

func (t Task) MarshalJSON() ([]byte, error)  { return json.Marshal(t.payload) }
func (t *Task) UnmarshalJSON(b []byte) error { return json.Unmarshal(b, &t.payload) }

func taskFromEnqueueOpts(t time.Time, job Job, opts mimir.EnqueueTaskOpts) (*Task, error) {
	task := Task{
		payload: taskPayload{
			ID:       xid.NewWithTime(t).String(),
			Name:     opts.Task,
			QueuedAt: t,
			Job:      job,
		},
	}

	if opts.Parameters != nil {
		var err error
		task.payload.Parameters.bytes, err = json.Marshal(opts.Parameters)
		if err != nil {
			return nil, err
		}
	}

	return &task, nil
}
