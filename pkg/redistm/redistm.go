package redistm

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"sort"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/rs/xid"
	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
)

const taskReportsKey = "taskReports"
const jobIdsKey = "job_ids"
const jobsKey = "jobs"

type RedisTaskManager struct {
	client *redis.Client
}

func NewRedisTaskManager(client *redis.Client) *RedisTaskManager {
	return &RedisTaskManager{
		client: client,
	}
}

// EnsureReady - runs healthcheck loop to wait until Redis is alive
func (rtm *RedisTaskManager) EnsureReady(ctx context.Context, opts mimir.RetryOpts) error {
	return mimir.Retry(ctx, func(ctx context.Context, ri mimir.RetryInfo) error {
		ping := rtm.client.Ping(ctx)
		if ping.Err() == nil {
			return nil
		}

		if ri.WillTryAgain {
			log.Warn().Int("try", ri.Try).Err(ping.Err()).Msgf("Unable to establish connection to Redis. Will try again in %v.", ri.NextCooldown)
		}

		return ping.Err()
	}, opts.ApplyDefaults())
}

func (rtm *RedisTaskManager) QueueJob(ctx context.Context, opts mimir.QueueJobOpts) (*mimir.QueueJobResult, error) {
	t := time.Now()

	jobInfo := JobInfo{
		payload: jobInfoPayload{
			jobPayload: jobPayload{
				ID:       xid.NewWithTime(t).String(),
				Name:     opts.Name,
				Internal: opts.Internal,
			},
			QueuedAt: t,
		},
	}

	job := Job{
		payload: jobInfo.payload.jobPayload,
	}

	jobInfoBytes, err := json.Marshal(jobInfo)
	if err != nil {
		return nil, err
	}

	result := mimir.QueueJobResult{
		Job:   job,
		Tasks: make([]mimir.Task, len(opts.Enqueue)),
	}

	_, pipelineErr := rtm.client.TxPipelined(context.TODO(), func(p redis.Pipeliner) error {
		// Register JobID as not finished (0)
		if !opts.Internal {
			p.ZAdd(context.TODO(), jobIdsKey, &redis.Z{Score: 0, Member: job.ID()})
		}

		// Add Job info into hash by its ID
		p.HSet(context.TODO(), jobsKey, job.ID(), jobInfoBytes)

		for i, enqueueOpts := range opts.Enqueue {
			task, pipeliner, err := buildEnqueueTransaction(job, enqueueOpts)
			if err != nil {
				return err
			}

			if err = pipeliner(p); err != nil {
				return err
			}

			result.Tasks[i] = task
		}

		return nil
	})

	if pipelineErr != nil {
		return nil, pipelineErr
	}

	return &result, nil
}

func (rtm *RedisTaskManager) QueueTask(ctx context.Context, opts mimir.QueueTaskOpts) ([]mimir.Task, error) {
	jobsByID, err := rtm.getJobsByIDs(ctx, opts.JobID)
	if err != nil {
		return nil, err
	}

	if jobInfoPayload, ok := jobsByID[opts.JobID]; ok {
		job := Job{payload: jobInfoPayload.jobPayload}

		tasks := make([]mimir.Task, len(opts.Enqueue))
		_, pipelineErr := rtm.client.TxPipelined(ctx, func(p redis.Pipeliner) error {
			for i, enqueueOpts := range opts.Enqueue {
				task, pipeliner, err := buildEnqueueTransaction(job, enqueueOpts)
				if err != nil {
					return err
				}

				err = pipeliner(p)
				if err != nil {
					return err
				}

				tasks[i] = task
			}

			return nil
		})

		if pipelineErr != nil {
			return nil, pipelineErr
		}

		return tasks, nil

	} else {
		return nil, nil
	}
}

func (rtm *RedisTaskManager) CancelTask(ctx context.Context, queue string, taskID string) error {
	sliceRes := rtm.client.LRange(ctx, queue, 0, -1)
	if sliceRes.Err() != nil {
		return sliceRes.Err()
	}

	for _, itemStr := range sliceRes.Val() {
		var task Task
		if err := json.Unmarshal([]byte(itemStr), &task); err != nil {
			continue
		}

		if task.ID() == taskID {
			taskReport := &TaskReport{
				payload: taskReportPayload{
					Task:  task,
					Error: NewTaskError(errors.New("cancelled")),
				},
			}

			return rtm.reportTask(*taskReport, &QueuedItem{
				// Note: intentionally mixed up queue (it is not in in unfinished one)
				UnfinishedQueue: queue,
				Payload:         itemStr,
			})
		}
	}

	return fmt.Errorf("task %v not found in queue %v", taskID, queue)
}

func (rtm *RedisTaskManager) GetJobs(ctx context.Context, opts mimir.GetJobsOpts) ([]mimir.JobInfo, error) {
	zrange := &redis.ZRangeBy{
		Count:  int64(opts.Count),
		Offset: int64(opts.Offset),
		Min:    "0",
		Max:    "1",
	}

	if opts.PendingOnly {
		zrange.Min = "0"
		zrange.Max = "0"
	}

	jobIDsRes := rtm.client.ZRangeByScoreWithScores(ctx, jobIdsKey, zrange)

	if jobIDsRes.Err() != nil {
		return nil, jobIDsRes.Err()
	}

	jobIDsWithScores := jobIDsRes.Val()

	if len(jobIDsWithScores) == 0 {
		return nil, nil
	}

	jobIDs := make([]string, len(jobIDsWithScores))
	for i, jobIDWithScore := range jobIDsWithScores {
		jobIDs[i] = jobIDWithScore.Member.(string)
	}

	jobInfoByID, err := rtm.getJobsByIDs(ctx, jobIDs...)
	if err != nil {
		return nil, err
	}

	var result []mimir.JobInfo
	for _, jobIDWithScore := range jobIDsWithScores {
		jobID := jobIDWithScore.Member.(string)
		if payload, ok := jobInfoByID[jobID]; ok {
			result = append(result, JobInfo{payload: payload, finished: jobIDWithScore.Score == 1})
		}
	}

	return result, nil
}

func (rtm *RedisTaskManager) ProcessQueue(ctx context.Context, opts mimir.ProcessQueueOpts) error {
	key := opts.QueueName
	unfinishedKey := fmt.Sprintf("%v_unfinished", opts.QueueName)

	// Move unfinished items back to queue
	err := rtm.requeueUnfinishedItems(ctx, key, unfinishedKey)
	if err != nil {
		return err
	}

	// Idle state
	isIdle := false

	processingSummary := &mimir.ProcessingSummary{}

	for {
		// Note: replace with BLMove once the redis library is more up to date (it replaces BRPOPLPUSH in redis >6.2)
		res := rtm.client.BRPopLPush(ctx, key, unfinishedKey, opts.IdleTimeout)

		if res.Err() == redis.Nil {
			if !isIdle {
				isIdle = true
				if opts.IdleHandler != nil {
					opts.IdleHandler(*processingSummary)
				}
				processingSummary = &mimir.ProcessingSummary{}
			}

			continue
		} else if res.Err() != nil {
			return res.Err()
		}

		// We have an item, lets update the queue state as not idle
		isIdle = false
		processingSummary.NumProcessed++

		// Initialize handle request
		handleReq := HandleRequest{
			queuedItem: &QueuedItem{
				Payload:         res.Val(),
				UnfinishedQueue: unfinishedKey,
			},
		}

		handleReq.error = json.Unmarshal([]byte(handleReq.queuedItem.Payload), &handleReq.task)
		if handleReq.error == nil {
			handleReq.job = ActiveJob{
				Job: handleReq.task.payload.Job,
				rtm: rtm,
			}
		}

		taskReport, err := rtm.handleTask(handleReq, opts.Handler)
		if err != nil {
			return err
		}

		if taskReport != nil {
			processingSummary.TimeSpent += taskReport.Took()
			if taskReport.Error() == nil {
				processingSummary.NumSucceeded++
			}

			if opts.OnResolved != nil {
				opts.OnResolved(taskReport)
			}
		}
	}
}

func (rtm *RedisTaskManager) ProcessTaskReports(ctx context.Context, opts mimir.ProcessTaskReportsOpts) error {
	key := taskReportsKey
	unfinishedKey := fmt.Sprintf("%v_unfinished", taskReportsKey)

	// Move unfinished items back to queue
	err := rtm.requeueUnfinishedItems(ctx, key, unfinishedKey)
	if err != nil {
		return err
	}

	for {
		// Note: replace with BLMove once the redis library is more up to date (it replaces BRPOPLPUSH in redis >6.2)
		// Take 1 element from the queue, while backing it up to the unfinished queue at the same time
		res := rtm.client.BRPopLPush(ctx, key, unfinishedKey, 5*time.Second)

		if res.Err() == redis.Nil {
			// On timeout, just continue with a next attempt
			continue
		} else if res.Err() != nil {
			return res.Err()
		}

		taskReportRaw := res.Val()

		// Process the task report
		var taskReport TaskReport
		err := json.Unmarshal([]byte(taskReportRaw), &taskReport)
		if err != nil {
			log.Error().Err(err).Msg("Unable to parse task report")
			continue
		}

		now := time.Now()

		msg := jobUpdateMessage{
			JobID:      taskReport.Job().ID(),
			TaskID:     taskReport.Task().ID(),
			TaskName:   taskReport.Task().Name(),
			FinishedAt: &now,
		}

		if taskReport.Error() != nil {
			msg.State = ts_FailedTask
		} else {
			msg.State = ts_FinishedTask
			took := int(taskReport.Took().Milliseconds())
			msg.Took = &took
		}

		msgBytes, err := json.Marshal(msg)
		if err != nil {
			return err
		}

		err = opts.Handler(&taskReport)
		if err == nil {
			jobPendingTasksKey := fmt.Sprintf("job_tasks:%v", taskReport.Job().ID())
			jobTasksTotalKey := fmt.Sprintf("job_tasks_total:%v", taskReport.Job().ID())
			jobTasksFailedKey := fmt.Sprintf("job_tasks_failed:%v", taskReport.Job().ID())
			jobTasksDurationKey := fmt.Sprintf("job_tasks_duration:%v", taskReport.Job().ID())

			composedTaskID := fmt.Sprintf("%v:%v", taskReport.Task().ID(), taskReport.Task().Name())
			var jobFinished bool

			// Mark job as finished if this is the last pending task
			for i := 0; ; i++ {
				err := rtm.client.Watch(context.TODO(), func(tx *redis.Tx) error {
					jobFinished = false

					countRes := tx.ZCount(context.TODO(), jobPendingTasksKey, "-inf", "0")
					if countRes.Err() != nil {
						return countRes.Err()
					}

					// If there is more than just one pending task, we are done
					if countRes.Val() > 1 {
						return nil
					}

					// Sanity check: make sure that the remaining pending task is ours
					pendingTasks := tx.ZRangeByScore(context.TODO(), jobPendingTasksKey, &redis.ZRangeBy{
						Min: "-inf",
						Max: "0",
					})

					if pendingTasks.Err() != nil {
						return pendingTasks.Err()
					}

					if len(pendingTasks.Val()) != 1 || pendingTasks.Val()[0] != composedTaskID {
						return nil
					}

					// Get job info
					jobInfoRes := tx.HGet(context.TODO(), jobsKey, taskReport.Job().ID())
					if jobInfoRes.Err() != nil {
						return jobInfoRes.Err()
					}

					// Get total number of tasks
					jobTasksTotalRes := tx.ZRangeWithScores(context.TODO(), jobTasksTotalKey, 0, -1)
					if jobTasksTotalRes.Err() != nil {
						return pendingTasks.Err()
					}

					var summary []taskSummaryPayload
					for _, item := range jobTasksTotalRes.Val() {
						summary = append(summary, taskSummaryPayload{
							Name:  item.Member.(string),
							Total: int(item.Score),
						})
					}

					// Get number of failed tasks
					jobTasksFailedRes := tx.ZRangeWithScores(context.TODO(), jobTasksFailedKey, 0, -1)
					if jobTasksFailedRes.Err() != nil {
						return jobTasksFailedRes.Err()
					}

					for _, item := range jobTasksFailedRes.Val() {
						for i, taskSummary := range summary {
							if taskSummary.Name == item.Member.(string) {
								summary[i].Failed = int(item.Score)
							}
						}
					}

					// Get time it took to process the tasks
					jobTasksDurationRes := tx.ZRangeWithScores(context.TODO(), jobTasksDurationKey, 0, -1)
					if jobTasksDurationRes.Err() != nil {
						return jobTasksDurationRes.Err()
					}

					for _, item := range jobTasksDurationRes.Val() {
						for i, taskSummary := range summary {
							if taskSummary.Name == item.Member.(string) {
								summary[i].Took = int(item.Score)
							}
						}
					}

					sort.Slice(summary, func(i, j int) bool {
						return summary[i].Name < summary[j].Name
					})

					var jobInfo jobInfoPayload
					err := json.Unmarshal([]byte(jobInfoRes.Val()), &jobInfo)
					if err != nil {
						return err
					}

					jobInfo.FinishedAt = &now
					jobInfo.TaskSummary = summary

					updatedJobInfoBytes, err := json.Marshal(jobInfo)
					if err != nil {
						return err
					}

					_, err = tx.TxPipelined(context.TODO(), func(p redis.Pipeliner) error {
						// Update job info
						p.HSet(context.TODO(), jobsKey, taskReport.Job().ID(), updatedJobInfoBytes)

						// Update job state in the JobID set
						if !jobInfo.Internal {
							p.ZAddCh(context.TODO(), jobIdsKey, &redis.Z{
								Score:  1, /* FINISHED */
								Member: taskReport.Job().ID(),
							})
						}

						return nil
					})

					if err == nil {
						jobFinished = true
					}

					return err
				}, jobPendingTasksKey)

				if err != redis.TxFailedErr {
					if err == nil {
						break
					} else {
						return err
					}
				} else if i < 5 {
					log.Warn().Msg("Concurrent update detected, trying again")
				} else {
					err := errors.New("too many concurrent writes, giving up")
					log.Error().Err(err).Msg("Unable to update job state")
					return err
				}
			}

			_, pipelineErr := rtm.client.TxPipelined(context.TODO(), func(p redis.Pipeliner) error {
				p.LRem(context.TODO(), unfinishedKey, -1, taskReportRaw)
				if jobFinished {
					p.Del(context.TODO(), jobPendingTasksKey, jobTasksTotalKey, jobTasksFailedKey, jobTasksDurationKey)
				} else if taskReport.Error() != nil {
					// Set task as failed
					p.ZAddXX(context.TODO(), jobPendingTasksKey, &redis.Z{
						Score:  2,
						Member: composedTaskID,
					})

					// Count it as failed
					p.ZIncrBy(context.TODO(), jobTasksFailedKey, 1, taskReport.Task().Name())
				} else {
					// Set task as finished
					p.ZAddXX(context.TODO(), jobPendingTasksKey, &redis.Z{
						Score:  1,
						Member: composedTaskID,
					})

					// Count the time it took
					p.ZIncrBy(context.TODO(), jobTasksDurationKey, float64(taskReport.Took().Milliseconds()), taskReport.Task().Name())
				}

				p.Publish(context.TODO(), "pending_tasks_updates", msgBytes)

				return nil
			})

			if pipelineErr != nil {
				return pipelineErr
			}

			if opts.OnResolved != nil {
				opts.OnResolved(&taskReport)
			}
		}
	}
}

func (rtm *RedisTaskManager) handleTask(req HandleRequest, handler mimir.TaskHandler) (mimir.TaskReport, error) {
	start := time.Now()
	err := handler(&req)
	timeSpent := time.Since(start)

	if req.error == nil {
		taskReport := &TaskReport{
			payload: taskReportPayload{
				Task:      req.task,
				Took:      timeSpent,
				Processor: req.processor,
			},
		}

		if err != nil {
			taskReport.payload.Error = NewTaskError(err)
		}

		if reportErr := rtm.reportTask(*taskReport, req.queuedItem); err != nil {
			return nil, reportErr
		}

		return taskReport, nil
	}

	// Note: we do not return error, as we do not treat handler errors as fatal
	return nil, nil
}

func (rtm *RedisTaskManager) reportTask(taskReport TaskReport, queuedItem *QueuedItem) error {
	taskReportBytes, err := json.Marshal(taskReport)
	if err != nil {
		return err
	}

	_, pipelineErr := rtm.client.TxPipelined(context.TODO(), func(p redis.Pipeliner) error {

		// Remove item from the unfinished queue
		if queuedItem != nil {
			p.LRem(context.TODO(), queuedItem.UnfinishedQueue, -1, queuedItem.Payload)
		}

		p.LPush(context.TODO(), taskReportsKey, taskReportBytes)

		return nil
	})

	if pipelineErr != nil {
		log.Error().Err(pipelineErr).Msg("Failed to update Redis queue after finished task")
		return pipelineErr
	}

	return nil
}

func (rtm *RedisTaskManager) requeueUnfinishedItems(ctx context.Context, queue string, unfinishedQueue string) error {
	for {
		res := rtm.client.RPopLPush(ctx, unfinishedQueue, queue)
		if res.Err() == redis.Nil {
			break
		} else if res.Err() != nil {
			return res.Err()
		}
	}

	return nil
}

func (rtm *RedisTaskManager) getJobsByIDs(ctx context.Context, jobIDs ...string) (map[string]jobInfoPayload, error) {
	jobsRes := rtm.client.HMGet(ctx, jobsKey, jobIDs...)

	if jobsRes.Err() != nil {
		return nil, jobsRes.Err()
	}

	result := make(map[string]jobInfoPayload)
	for _, jobData := range jobsRes.Val() {
		if jobData != nil {
			var payload jobInfoPayload
			err := json.Unmarshal([]byte(jobData.(string)), &payload)
			if err != nil {
				return nil, err
			}

			result[payload.ID] = payload
		}
	}

	return result, nil
}
