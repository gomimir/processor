package redistm

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/tevino/abool"
	mimir "gitlab.com/gomimir/processor"
)

type taskState string

const (
	ts_PendingTask  taskState = "PENDING"
	ts_FailedTask   taskState = "FAILED"
	ts_FinishedTask taskState = "FINISHED"
)

type jobUpdateMessage struct {
	JobID    string `json:"jobId"`
	TaskID   string `json:"taskId"`
	TaskName string `json:"taskName"`

	State taskState `json:"state"`

	FinishedAt *time.Time `json:"finishedAt,omitempty"`
	Took       *int       `json:"took,omitempty"`
}

type JobMonitorEvent struct {
	err       error
	job       mimir.JobInfo
	state     *JobProgress
	prevState *JobProgress
}

func (jme JobMonitorEvent) Error() error       { return jme.err }
func (jme JobMonitorEvent) Job() mimir.JobInfo { return jme.job }

func (jme JobMonitorEvent) State() mimir.JobState {
	// nil conversion
	if jme.state != nil {
		return jme.state
	}

	return nil
}

func (jme JobMonitorEvent) PrevState() mimir.JobState {
	// nil conversion
	if jme.prevState != nil {
		return jme.prevState
	}

	return nil
}

type JobMonitorSubscription struct {
	eventC chan mimir.JobMonitorEvent
	closeC chan error
	closed *abool.AtomicBool
}

func (jms *JobMonitorSubscription) Send(evt mimir.JobMonitorEvent) {
	if jms.closed.IsNotSet() {
		jms.eventC <- evt
	}
}

func (jms *JobMonitorSubscription) Close(err error) {
	if jms.closed.SetToIf(true, false) {
		if err != nil {
			jms.eventC <- JobMonitorEvent{err: err}
		}

		close(jms.eventC)
		close(jms.closeC)
	}
}

type JobMonitor struct {
	rtm *RedisTaskManager

	subscribersMU sync.RWMutex
	subscribers   map[*JobMonitorSubscription]struct{}

	stateMU sync.RWMutex
	state   map[string]*JobProgress

	readyC chan struct{}
	closeC chan struct{}
	err    atomic.Value
}

func (jm *JobMonitor) GetJobState(ctx context.Context, jobID string) (mimir.JobState, error) {
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	case <-jm.readyC:
	}

	if err, ok := jm.err.Load().(error); ok && err != nil {
		return nil, err
	}

	jm.stateMU.RLock()
	jobState := jm.state[jobID]
	jm.stateMU.RUnlock()

	if jobState != nil {
		return jobState, nil
	}

	return nil, nil
}

func (jm *JobMonitor) Subscribe(ctx context.Context) <-chan mimir.JobMonitorEvent {
	eventC := make(chan mimir.JobMonitorEvent)

	go func() {
		if err, ok := jm.err.Load().(error); ok && err != nil {
			eventC <- JobMonitorEvent{err: err}
			close(eventC)
			return
		}

		sub := &JobMonitorSubscription{
			eventC: eventC,
			closeC: make(chan error),
			closed: abool.New(),
		}

		jm.subscribersMU.Lock()
		jm.subscribers[sub] = struct{}{}
		jm.subscribersMU.Unlock()

		select {
		case <-ctx.Done():
			sub.Close(ctx.Err())
		case <-sub.closeC:
		}

		jm.subscribersMU.Lock()
		delete(jm.subscribers, sub)
		jm.subscribersMU.Unlock()
	}()

	return eventC
}

func (jm *JobMonitor) Broadcast(evt mimir.JobMonitorEvent) {
	jm.subscribersMU.RLock()

	for sub := range jm.subscribers {
		go sub.Send(evt)
	}

	jm.subscribersMU.RUnlock()
}

func (jm *JobMonitor) Close(err error) {
	jm.subscribersMU.RLock()

	for sub := range jm.subscribers {
		go sub.Close(err)
	}

	jm.subscribersMU.RUnlock()

	jm.err.Store(err)
	close(jm.closeC)
}

func (jm *JobMonitor) Done() <-chan error {
	errorC := make(chan error)

	go func() {
		<-jm.closeC
		errorC <- jm.err.Load().(error)
	}()

	return errorC
}

func (jm *JobMonitor) HandleRedisMessageChunk(chunk []*redis.Message) error {
	for _, msg := range chunk {
		err := jm.HandleRedisMessage(msg)
		if err != nil {
			return err
		}
	}

	return nil
}

func (jm *JobMonitor) HandleRedisMessage(msg *redis.Message) error {
	var update jobUpdateMessage
	err := json.Unmarshal([]byte(msg.Payload), &update)
	if err != nil {
		// We skip malformed messages, there is nothing we can do for them anyway
		return nil
	}

	jm.stateMU.RLock()
	prevJobProgress, ok := jm.state[update.JobID]
	jm.stateMU.RUnlock()

	jobProgress := prevJobProgress

	if !ok {
		if update.State == ts_PendingTask {
			jobByIDs, err := jm.rtm.getJobsByIDs(context.TODO(), update.JobID)
			if err != nil {
				return err
			}

			if payload, ok := jobByIDs[update.JobID]; ok {
				jobProgress = &JobProgress{job: JobInfo{payload: payload}}
				jm.stateMU.Lock()
				jm.state[update.JobID] = jobProgress
				jm.stateMU.Unlock()
			} else {
				// Put tombstone in place of ID which we failed to find
				jm.stateMU.Lock()
				jm.state[update.JobID] = nil
				jm.stateMU.Unlock()
				return nil
			}
		} else {
			return nil
		}
	} else if prevJobProgress == nil {
		// Fast skip over tombstones
		return nil
	}

	newProgress, hasChanged := jobProgress.WithTaskUpdate(update.TaskName, update.TaskID, update)
	if hasChanged {
		if !jobProgress.Finished() && newProgress.Finished() {
			newProgress.job.payload.FinishedAt = update.FinishedAt
		}

		jm.stateMU.Lock()
		jm.state[update.JobID] = &newProgress
		jm.stateMU.Unlock()

		job := newProgress.job
		job.finished = newProgress.Finished()

		jm.Broadcast(JobMonitorEvent{
			job:       job,
			state:     &newProgress,
			prevState: prevJobProgress,
		})
	}

	return nil
}

// --------------------------------

func (rtm *RedisTaskManager) MonitorJobs(ctx context.Context) (mimir.JobMonitor, error) {
	sub := rtm.client.Subscribe(ctx, "pending_tasks_updates")
	iface, err := sub.Receive(ctx)
	if err != nil {
		return nil, err
	} else {
		jm := &JobMonitor{
			rtm:         rtm,
			subscribers: make(map[*JobMonitorSubscription]struct{}),
			readyC:      make(chan struct{}),
			closeC:      make(chan struct{}),
		}

		go func() {

			chunkC := make(chan []*redis.Message)
			errC := make(chan error)
			readyToConsumeC := make(chan struct{})

			consumerCtx, consumerCancel := context.WithCancel(ctx)
			defer consumerCancel()

			producerCtx, producerCancel := context.WithCancel(ctx)
			defer producerCancel()

			// Consumer
			go func() {
				var err error
				jm.state, err = rtm.getInitialMonitoringState(consumerCtx)
				if err != nil {
					producerCancel()
					errC <- err
					return
				}

				readyToConsumeC <- struct{}{}
				close(jm.readyC)

				for {
					select {
					case chunk := <-chunkC:
						jm.HandleRedisMessageChunk(chunk)

						if err != nil {
							producerCancel()
							errC <- err
							return
						} else {
							readyToConsumeC <- struct{}{}
						}
					case <-consumerCtx.Done():
						return
					}
				}
			}()

			// Producer
			go func() {
				var chunk []*redis.Message

				if m, ok := iface.(*redis.Message); ok {
					chunk = append(chunk, m)
				}

				messageC := sub.Channel()
				isReady := false

				for {
					select {
					case <-readyToConsumeC:
						if len(chunk) > 0 {
							chunkC <- chunk
							chunk = nil
						} else {
							isReady = true
						}

					case m := <-messageC:
						if m != nil {
							chunk = append(chunk, m)
							if isReady {
								isReady = false
								chunkC <- chunk
								chunk = nil
							}
						}

					case <-producerCtx.Done():
						return
					}
				}
			}()

			select {
			case err := <-errC:
				jm.Close(err)
			case <-ctx.Done():
				jm.Close(ctx.Err())
			}
		}()

		return jm, nil
	}
}

func (rtm *RedisTaskManager) getInitialMonitoringState(ctx context.Context) (map[string]*JobProgress, error) {
	state := make(map[string]*JobProgress)

	pendingJobs, err := rtm.GetJobs(ctx, mimir.GetJobsOpts{
		PendingOnly: true,
	})

	if err != nil {
		return nil, err
	}

	for _, job := range pendingJobs {
		state[job.ID()] = &JobProgress{
			job: job.(JobInfo),
		}

		var jobTasksRes *redis.ZSliceCmd
		var jobTaskDuration *redis.ZSliceCmd
		_, err := rtm.client.TxPipelined(ctx, func(p redis.Pipeliner) error {
			jobTasksRes = p.ZRangeWithScores(ctx, fmt.Sprintf("job_tasks:%v", job.ID()), 0, -1)
			jobTaskDuration = p.ZRangeWithScores(ctx, fmt.Sprintf("job_tasks_duration:%v", job.ID()), 0, -1)
			return nil
		})

		if err != nil {
			return nil, err
		}

		for _, item := range jobTasksRes.Val() {
			tokens := strings.Split(item.Member.(string), ":")
			if len(tokens) == 2 {
				taskID := tokens[0]
				taskName := tokens[1]

				var ts taskState
				if item.Score == 1 {
					ts = ts_FinishedTask
				} else if item.Score == 2 {
					ts = ts_FailedTask
				} else {
					ts = ts_PendingTask
				}

				newProgress, _ := state[job.ID()].WithTaskUpdate(taskName, taskID, jobUpdateMessage{
					JobID:    job.ID(),
					TaskID:   taskID,
					TaskName: taskName,
					State:    ts,
				})
				state[job.ID()] = &newProgress
			}
		}

		for _, item := range jobTaskDuration.Val() {
			taskName := item.Member.(string)
			totalDuration := int(item.Score)

			if tp, ok := state[job.ID()].progressByTask[taskName]; ok {
				tp.took += totalDuration
				state[job.ID()].progressByTask[taskName] = tp
			}
		}
	}

	return state, nil
}
