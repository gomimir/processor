package redistm

import (
	"fmt"
	"sort"

	mimir "gitlab.com/gomimir/processor"
)

type TaskProgress struct {
	taskName   string
	numPending int
	numFailed  int
	took       int
	tasks      map[string]taskState
}

func (tp TaskProgress) Name() string {
	return tp.taskName
}

func (tp TaskProgress) Finished() bool {
	return tp.numPending == 0
}

func (tp TaskProgress) Total() int {
	return len(tp.tasks)
}

func (tp TaskProgress) Pending() int {
	return tp.numPending
}

func (tp TaskProgress) Failed() int {
	return tp.numFailed
}

func (tp TaskProgress) Took() int {
	return tp.took
}

func (tp TaskProgress) WithTaskUpdate(taskID string, update jobUpdateMessage) (TaskProgress, bool) {
	existingState, alreadyExisting := tp.tasks[taskID]

	if alreadyExisting && existingState == update.State {
		return tp, false
	}

	newTasks := make(map[string]taskState)
	for k, v := range tp.tasks {
		newTasks[k] = v
	}
	newTasks[taskID] = update.State
	tp.tasks = newTasks

	if update.State == ts_PendingTask {
		tp.numPending++
	} else if alreadyExisting {
		tp.numPending--
	}

	if update.State == ts_FailedTask {
		tp.numFailed++
	}

	if update.Took != nil {
		tp.took += *update.Took
	}

	return tp, true
}

func (tp TaskProgress) String() string {
	return fmt.Sprintf("%v/%v", tp.Total()-tp.Pending(), tp.Total())
}

type JobProgress struct {
	job JobInfo
	// Sorted list of all task names
	taskNames []string
	// Summary of progress for individual tasks (by their name)
	progressByTask map[string]TaskProgress
}

func (jp JobProgress) Finished() bool {
	for _, tp := range jp.progressByTask {
		if !tp.Finished() {
			return false
		}
	}

	return true
}

func (jp JobProgress) WithTaskUpdate(taskName string, taskID string, update jobUpdateMessage) (JobProgress, bool) {
	taskProgress, alreadyExisting := jp.progressByTask[taskName]

	if alreadyExisting {
		var hasChanged bool
		taskProgress, hasChanged = taskProgress.WithTaskUpdate(taskID, update)
		if !hasChanged {
			return jp, false
		}
	} else {
		newTaskNames := make([]string, len(jp.taskNames)+1)
		i := sort.SearchStrings(jp.taskNames, taskName)
		if i > 0 {
			copy(newTaskNames, jp.taskNames[:i])
		}
		copy(newTaskNames[i+1:], jp.taskNames[i:])
		newTaskNames[i] = taskName
		jp.taskNames = newTaskNames

		taskProgress, _ = taskProgress.WithTaskUpdate(taskID, update)
		taskProgress.taskName = taskName
	}

	newProgressByTask := make(map[string]TaskProgress)
	for k, tp := range jp.progressByTask {
		newProgressByTask[k] = tp
	}

	newProgressByTask[taskName] = taskProgress
	jp.progressByTask = newProgressByTask

	return jp, true
}

func (jp JobProgress) TaskProgress() []mimir.TaskProgress {
	count := len(jp.taskNames)
	if count > 0 {
		result := make([]mimir.TaskProgress, len(jp.taskNames))
		for i, taskName := range jp.taskNames {
			result[i] = jp.progressByTask[taskName]
		}

		return result
	}

	return nil
}

func (jp JobProgress) String() string {
	if len(jp.taskNames) > 0 {
		var result string
		for _, taskName := range jp.taskNames {
			result += fmt.Sprintf(",%v(%v)", taskName, jp.progressByTask[taskName].String())
		}
		return result[1:]
	}

	return ""
}
