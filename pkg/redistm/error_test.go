package redistm_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	redistm "gitlab.com/gomimir/processor/pkg/redistm"
)

func TestTaskErrorPayload(t *testing.T) {
	taskErr := redistm.NewTaskError(fmt.Errorf("here comes the error"))

	assert.Equal(t, "here comes the error", taskErr.Error())

	bytes, err := json.Marshal(taskErr)
	if assert.NoError(t, err) {
		var taskErr2 redistm.TaskError
		err = json.Unmarshal(bytes, &taskErr2)
		if assert.NoError(t, err) {
			assert.Equal(t, *taskErr, taskErr2)
		}
	}
}
