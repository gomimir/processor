package redistm

import (
	"encoding/json"

	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type taskErrorPayload struct {
	Code    string `json:"code,omitempty"`
	Message string `json:"message"`
}

type TaskError struct {
	payload taskErrorPayload
}

func (te TaskError) Code() string  { return te.payload.Code }
func (te TaskError) Error() string { return te.payload.Message }

func (te TaskError) MarshalJSON() ([]byte, error)  { return json.Marshal(te.payload) }
func (te *TaskError) UnmarshalJSON(b []byte) error { return json.Unmarshal(b, &te.payload) }

func NewTaskError(err error) *TaskError {
	te := &TaskError{
		payload: taskErrorPayload{
			Message: err.Error(),
		},
	}

	if x, ok := status.FromError(err); ok {
		if x.Code() == codes.DeadlineExceeded {
			te.payload.Message = x.Message()
			te.payload.Code = "DEADLINE_EXCEEDED"
		} else if x.Code() == codes.Unavailable {
			te.payload.Message = x.Message()
			te.payload.Code = "UNAVAILABLE"
		} else {
			for _, d := range x.Details() {
				if d, ok := d.(*errdetails.ErrorInfo); ok {
					te.payload.Message = x.Message()
					te.payload.Code = d.Reason
					break
				}
			}
		}

	} else if x, ok := err.(interface{ Code() string }); ok {
		te.payload.Code = x.Code()
	}

	return te
}
