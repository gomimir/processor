package redistm

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
	mimir "gitlab.com/gomimir/processor"
)

type ActiveJob struct {
	Job
	rtm *RedisTaskManager
}

func (j ActiveJob) EnqueueTask(opts mimir.EnqueueTaskOpts) (mimir.Task, error) {
	task, pipeliner, err := buildEnqueueTransaction(j.Job, opts)
	if err != nil {
		return nil, err
	}

	_, pipelineErr := j.rtm.client.TxPipelined(context.TODO(), pipeliner)
	if pipelineErr != nil {
		return nil, pipelineErr
	}

	return task, nil
}

func buildEnqueueTransaction(j Job, opts mimir.EnqueueTaskOpts) (*Task, func(p redis.Pipeliner) error, error) {
	task, err := taskFromEnqueueOpts(time.Now(), j, opts)
	if err != nil {
		return nil, nil, err
	}

	bytes, err := json.Marshal(task)
	if err != nil {
		return nil, nil, err
	}

	upd, err := json.Marshal(jobUpdateMessage{
		JobID:    j.ID(),
		TaskID:   task.ID(),
		TaskName: task.Name(),
		State:    ts_PendingTask,
	})

	if err != nil {
		return nil, nil, err
	}

	return task, func(p redis.Pipeliner) error {
		p.LPush(context.TODO(), opts.Queue, bytes)
		p.ZAdd(context.TODO(), fmt.Sprintf("job_tasks:%v", j.ID()), &redis.Z{
			Score:  0,
			Member: fmt.Sprintf("%v:%v", task.ID(), task.Name()),
		})
		p.ZIncrBy(context.TODO(), fmt.Sprintf("job_tasks_total:%v", j.ID()), 1, task.Name())
		p.Publish(context.TODO(), "pending_tasks_updates", upd)

		return nil
	}, nil
}
