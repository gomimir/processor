package redistm

import (
	"encoding/json"
	"time"

	mimir "gitlab.com/gomimir/processor"
)

type taskReportPayload struct {
	Task Task `json:"task"`

	Error *TaskError    `json:"error,omitempty"`
	Took  time.Duration `json:"took,omitempty"`

	Processor *mimir.AppInfo `json:"processor,omitempty"`
}

type TaskReport struct {
	payload taskReportPayload
}

func (report *TaskReport) Job() mimir.Job   { return report.payload.Task.payload.Job }
func (report *TaskReport) Task() mimir.Task { return report.payload.Task }
func (report *TaskReport) Error() mimir.TaskError {
	// nil conversion
	if report.payload.Error != nil {
		return report.payload.Error
	}

	return nil
}
func (report *TaskReport) Took() time.Duration       { return report.payload.Took }
func (report *TaskReport) Processor() *mimir.AppInfo { return report.payload.Processor }

func (report TaskReport) MarshalJSON() ([]byte, error)  { return json.Marshal(report.payload) }
func (report *TaskReport) UnmarshalJSON(b []byte) error { return json.Unmarshal(b, &report.payload) }
