package mimir

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/processor/pkg/redistm"
)

// RunOpts - options for Run
type RunOpts struct {
	Context context.Context

	RedisAddr     string
	RedisPassword string
	RedisDatabase int

	QueueName    string
	TaskHandlers map[string]mimir.TaskHandler
	Processor    *mimir.AppInfo
}

// Run - quick stub for rapid processor app development
// Contains predefined main loop for task processing
func Run(opts RunOpts) {
	// Apply default options
	if opts.Context == nil {
		opts.Context = context.TODO()
	}

	if opts.RedisAddr == "" {
		opts.RedisAddr = "localhost:6379"
	}

	if opts.QueueName == "" {
		log.Fatal().Msg("Invalid queue name")
		os.Exit(1)
	}

	if len(opts.TaskHandlers) == 0 {
		log.Fatal().Msg("No task handlers registered")
		os.Exit(1)
	}

	// Initialize Redis client
	redisClient := redis.NewClient(&redis.Options{
		Addr:     opts.RedisAddr,
		Password: opts.RedisPassword,
		DB:       opts.RedisDatabase,
	})

	// Create task manager
	tm := redistm.NewRedisTaskManager(redisClient)

	// Ping the Redis server to ensure we are good to go
	err := tm.EnsureReady(opts.Context, mimir.RetryOpts{})
	if err != nil {
		log.Fatal().Err(err).Msg("Unable to establish connection to Redis")
		os.Exit(1)
	}

	err = tm.ProcessQueue(opts.Context, mimir.ProcessQueueOpts{
		QueueName: opts.QueueName,
		Processor: opts.Processor,

		Handler: func(req mimir.HandleRequest) error {
			sublog := log.With().Str("task", req.Task().Name()).Str("task_id", req.Task().ID()).Logger()

			if handler, ok := opts.TaskHandlers[req.Task().Name()]; ok {
				start := time.Now()
				err := handler(req)
				timeSpent := time.Since(start)

				if err != nil {
					sublog.Error().Err(err).Msg("Task finished with error")
				} else {
					sublog.Info().Str("took", fmt.Sprintf("%v", timeSpent)).Msg("Task finished successfully")
				}

				return err
			}

			err := fmt.Errorf("no handler for task: %v", req.Task().Name())
			sublog.Error().Err(err).Msg("Failed to process queued task")

			return err
		},

		IdleTimeout: 5 * time.Second,
		IdleHandler: func(summary mimir.ProcessingSummary) {
			log.Info().Msgf("Idle. Processed %v tasks (%v failed) in %v since last idle state.",
				summary.NumProcessed,
				summary.NumProcessed-summary.NumSucceeded,
				summary.TimeSpent,
			)
		},
	})

	if err != nil {
		log.Fatal().Err(err).Msg("Queue processing stopped")
		os.Exit(1)
	}
}
