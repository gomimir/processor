package mimir

type VideoFileFormat string

const (
	VF_MP4 VideoFileFormat = "mp4"
)

func (f VideoFileFormat) FileSuffix() string {
	switch f {
	case VF_MP4:
		return "mp4"
	default:
		return string(f)
	}
}

func (f VideoFileFormat) DisplayName() string {
	switch f {
	case VF_MP4:
		return "MP4"
	default:
		return string(f)
	}
}

func (f VideoFileFormat) IsValid() bool {
	switch f {
	case VF_MP4:
		return true
	}

	return false
}
