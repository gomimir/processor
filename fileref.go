package mimir

// FileRef - Reference to file stored in a repository
type FileRef struct {
	// Name of the repository in which the file is stored
	Repository string `json:"repository" example:"minio"`

	// Filename, in case of S3 the first "directory" is a bucket name
	Filename string `json:"filename" example:"photos/somedir/somephoto.jpg"`
}
