package mimir_test

import (
	"testing"

	"github.com/icza/gox/gox"
	"github.com/stretchr/testify/assert"
	mimir "gitlab.com/gomimir/processor"
)

func TestThumbnailFit(t *testing.T) {
	thumb := mimir.ThumbnailDescriptor{
		Width:  1200,
		Height: 800,
	}

	assert.Equal(t, 1.0, thumb.ScoreDimensionFit(gox.NewInt(1200), gox.NewInt(800)))
	assert.Greater(t, 1.0, thumb.ScoreDimensionFit(gox.NewInt(800), gox.NewInt(800)))
	assert.Greater(t, thumb.ScoreDimensionFit(gox.NewInt(800), gox.NewInt(800)), thumb.ScoreDimensionFit(gox.NewInt(600), gox.NewInt(800)))

	assert.Greater(t, 0.0, thumb.ScoreDimensionFit(gox.NewInt(1600), gox.NewInt(800)))
	assert.Greater(t, thumb.ScoreDimensionFit(gox.NewInt(1600), gox.NewInt(800)), thumb.ScoreDimensionFit(gox.NewInt(2000), gox.NewInt(800)))
}
