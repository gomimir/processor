package mimir

import (
	"encoding/json"
	"fmt"
	"reflect"
	"regexp"
	"strconv"
)

// VersionInfo - Object containing version parsed version information according to format used in mimir apps
type VersionInfo struct {
	Major int
	Minor int
	Patch int

	str     string
	isValid bool
}

var versionRX = regexp.MustCompile(`^([0-9]+).([0-9]+).([0-9]+)`)

func VersionInfoFromStr(str string) (VersionInfo, error) {
	m := versionRX.FindStringSubmatch(str)
	parsed := VersionInfo{
		str: str,
	}

	if len(m) == 4 {
		var err error

		if parsed.Major, err = strconv.Atoi(m[1]); err != nil {
			return parsed, fmt.Errorf("unable to parse major version number")
		}

		if parsed.Minor, err = strconv.Atoi(m[2]); err != nil {
			return parsed, fmt.Errorf("unable to parse minor version number")
		}

		if parsed.Patch, err = strconv.Atoi(m[3]); err != nil {
			return parsed, fmt.Errorf("unable to parse patch version number")
		}

	} else {
		return parsed, fmt.Errorf("unable to parse version numbers")
	}

	parsed.isValid = true
	return parsed, nil
}

func (vi VersionInfo) String() string {
	return vi.str
}

func (vi VersionInfo) MarshalJSON() ([]byte, error) {
	return json.Marshal(vi.String())
}

func (vi VersionInfo) IsValid() bool {
	return vi.isValid
}

func (vi VersionInfo) GTE(another VersionInfo) bool {
	if vi.IsValid() && another.IsValid() {
		return vi.Compare(another) >= 0
	}

	return false
}

func (vi VersionInfo) Compare(another VersionInfo) int {
	return CompareVersionInfo(vi, another)
}

// Compare - returns 0 if a==b, -1 if a < b, and +1 if a > b
func CompareVersionInfo(a, b VersionInfo) int {
	if !a.IsValid() {
		if !b.IsValid() {
			return 0
		} else {
			return -1
		}
	} else if !b.IsValid() {
		return 1
	}

	if a.Major < b.Major {
		return -1
	} else if a.Major > b.Major {
		return 1
	}

	if a.Minor < b.Minor {
		return -1
	} else if a.Minor > b.Minor {
		return 1
	}

	if a.Patch < b.Patch {
		return -1
	} else if a.Patch > b.Patch {
		return 1
	}

	return 0
}

func VersionInfoMapStructureHook(from reflect.Type, to reflect.Type, data interface{}) (interface{}, error) {
	if to == reflect.TypeOf(VersionInfo{}) {
		if versionStr, ok := data.(string); ok {
			return VersionInfoFromStr(versionStr)
		}

		return nil, fmt.Errorf("invalid version string")
	}

	return data, nil
}
