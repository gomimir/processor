package mimir

type ThumbnailFileFormat string

const (
	TF_JPEG ThumbnailFileFormat = "jpeg"
	TF_WebP ThumbnailFileFormat = "webp"
)

func (f ThumbnailFileFormat) FileSuffix() string {
	switch f {
	case TF_JPEG:
		return "jpg"
	default:
		return string(f)
	}
}

func (f ThumbnailFileFormat) DisplayName() string {
	switch f {
	case TF_JPEG:
		return "JPEG"
	case TF_WebP:
		return "WebP"
	default:
		return string(f)
	}
}

func (f ThumbnailFileFormat) IsValid() bool {
	switch f {
	case TF_JPEG, TF_WebP:
		return true
	}

	return false
}
