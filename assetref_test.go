package mimir_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	mimir "gitlab.com/gomimir/processor"
)

func TestAssetRefForKey(t *testing.T) {
	ref := mimir.AssetRefForKey("documents", "mydoc.pdf")
	assert.True(t, ref.IsValid())
	assert.Equal(t, "documents", ref.IndexName())
	assert.Equal(t, "hZ3qWzdo0dJh42E-PGUccpPbEaSCmfPgKyVG9g", ref.AssetID())
	assert.Equal(t, "ZG9jdW1lbnRzOoWd6ls3aNHSYeNhPjxlHHKT2xGkgpnz4CslRvY", ref.String())
}

func TestAssetRefForID(t *testing.T) {
	ref := mimir.AssetRefForID("documents", "hZ3qWzdo0dJh42E-PGUccpPbEaSCmfPgKyVG9g")
	assert.True(t, ref.IsValid())
	assert.Equal(t, "documents", ref.IndexName())
	assert.Equal(t, "hZ3qWzdo0dJh42E-PGUccpPbEaSCmfPgKyVG9g", ref.AssetID())
	assert.Equal(t, "ZG9jdW1lbnRzOoWd6ls3aNHSYeNhPjxlHHKT2xGkgpnz4CslRvY", ref.String())
}

func TestAssetRefFromValidString(t *testing.T) {
	ref := mimir.AssetRefFromString("ZG9jdW1lbnRzOoWd6ls3aNHSYeNhPjxlHHKT2xGkgpnz4CslRvY")
	assert.True(t, ref.IsValid())
	assert.Equal(t, "documents", ref.IndexName())
	assert.Equal(t, "hZ3qWzdo0dJh42E-PGUccpPbEaSCmfPgKyVG9g", ref.AssetID())
	assert.Equal(t, "ZG9jdW1lbnRzOoWd6ls3aNHSYeNhPjxlHHKT2xGkgpnz4CslRvY", ref.String())
}

func TestAssetRefForFromInvalidString(t *testing.T) {
	ref := mimir.AssetRefFromString("some garbage")
	assert.False(t, ref.IsValid())
	assert.Equal(t, "some garbage", ref.String())
}
