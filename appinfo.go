package mimir

type AppInfo struct {
	Name    string      `json:"name"`
	Version VersionInfo `json:"version"`
}
