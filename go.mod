module gitlab.com/gomimir/processor

go 1.16

require (
	github.com/alicebob/miniredis/v2 v2.14.3
	github.com/go-redis/redis/v8 v8.8.0
	github.com/golang/protobuf v1.4.3
	github.com/icza/gox v0.0.0-20210726201659-cd40a3f8d324
	github.com/mitchellh/mapstructure v1.4.1
	github.com/rs/xid v1.3.0
	github.com/rs/zerolog v1.20.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	github.com/tevino/abool v1.2.0
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
