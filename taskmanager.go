package mimir

import (
	"context"
	"time"
)

type Task interface {
	ID() string
	Name() string
	QueuedAt() time.Time
	Parameters() Parameters
}

type Job interface {
	ID() string
	Name() string
}

type Parameters interface {
	LoadTo(dst interface{}) error
	AsMap() (map[string]interface{}, error)
}

type EnqueueTaskOpts struct {
	Queue      string
	Task       string
	Parameters interface{}
}

type ActiveJob interface {
	Job
	EnqueueTask(opts EnqueueTaskOpts) (Task, error)
}

type JobInfo interface {
	Job
	QueuedAt() time.Time
	FinishedAt() *time.Time
	Finished() bool

	// Only for finished tasks
	TaskSummary() []TaskProgress
}

type TaskProgress interface {
	Name() string
	Finished() bool
	Total() int
	Pending() int
	Failed() int
	Took() int
}

type JobState interface {
	Finished() bool
	TaskProgress() []TaskProgress
}

type HandleRequest interface {
	Error() error
	Task() Task
	Job() ActiveJob
}

type TaskError interface {
	Code() string
	Error() string
}

type TaskReport interface {
	Job() Job
	Task() Task

	Error() TaskError
	Took() time.Duration

	Processor() *AppInfo
}

type TaskHandler = func(req HandleRequest) error
type ReportHandler = func(report TaskReport)
type ReportHandlerE = func(report TaskReport) error

type ProcessingSummary struct {
	NumProcessed int
	NumSucceeded int
	TimeSpent    time.Duration
}

type ProcessQueueOpts struct {
	QueueName string
	Handler   TaskHandler

	// Called after task handler has finished and task has been successfully reported
	OnResolved ReportHandler

	IdleTimeout time.Duration
	IdleHandler func(summary ProcessingSummary)

	Processor *AppInfo
}

type QueueJobOpts struct {
	Name     string
	Internal bool
	Enqueue  []EnqueueTaskOpts
}

type QueueJobResult struct {
	Job   Job
	Tasks []Task
}

type ProcessTaskReportsOpts struct {
	Handler ReportHandlerE

	// Called after report has been resolved
	OnResolved ReportHandler
}

type GetJobsOpts struct {
	Count       int
	Offset      int
	PendingOnly bool
}

type QueueTaskOpts struct {
	JobID   string
	Enqueue []EnqueueTaskOpts
}

type TaskManager interface {
	EnsureReady(ctx context.Context, opts RetryOpts) error

	QueueJob(ctx context.Context, opts QueueJobOpts) (*QueueJobResult, error)

	QueueTask(ctx context.Context, opts QueueTaskOpts) ([]Task, error)
	CancelTask(ctx context.Context, queue string, taskID string) error

	GetJobs(ctx context.Context, opts GetJobsOpts) ([]JobInfo, error)
	MonitorJobs(ctx context.Context) (JobMonitor, error)

	ProcessQueue(ctx context.Context, opts ProcessQueueOpts) error
	ProcessTaskReports(ctx context.Context, opts ProcessTaskReportsOpts) error
}

type JobMonitorEvent interface {
	Error() error

	Job() JobInfo
	State() JobState
	PrevState() JobState
}

type JobMonitor interface {
	GetJobState(ctx context.Context, jobID string) (JobState, error)
	Subscribe(ctx context.Context) <-chan JobMonitorEvent
	Done() <-chan error
}
